<!-- Hoja de Estilos de la librería dc.js -->
<link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/dc.min.css')?>"/>

<!-- Hoja de Estilos de la seccion de Resultados -->
<link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/chart.css')?>"/>

<!-- Gráfico Histórico de Resultados -->
<div class="col-sm-12" id="chart-resultados">
	<div id="a">
		<h1>Resultados</h1>
	</div >
</div>
<div class="col-sm-12" id="chart-filtro">
	
</div>


<!---->
<div class="col-sm-3" id="chart-applications">
    <h3>Aplicación</h3>
</div>

<!---->
<div class="col-sm-3" id="chart-actividades">
    <h3>Actividad</h3>
</div>

<!---->
<div class="col-sm-3" id="chart-ejercicios">
  <h3>Ejercicio</h3>
</div>

<!---->
<div class="col-sm-3" id="chart-respuestas">
  <h3>Respuestas</h3>
</div>

<!---->
<table id="data-table" class="table table-hover table-bordered table-condensed table-striped">
    <thead>
        <tr>
            <th>Curso</th>
            <th>Alumno</th>
			<th>Actividad</th>
            <th>Ejercicio</th>
            <th>Resultado</th>
            <th>Fecha</th>
        </tr>
    </thead>
    <tbody></tbody>
</table>

<!---->
<div class="col-sm-12 text-center" id="chart-alerta" style="padding-top: 20px;">
    <img class="img-thumbnail" src="https://cdn0.iconfinder.com/data/icons/smile-emoticons/78/smyle_emoticons-03-512.png" width="250">
    <div class="caption">
        <h3 class="text-danger">No se encontraron resultados.</h3>
        <a href="#" class="btn btn-primary" role="button" onclick="history.go(-1);"><i class="fa fa-arrow-left"></i> Regresar</a>
    </div>
</div>

<!---->
<script type="text/javascript" src="<?=base_url('assets/datatable/js/jquery.dataTables.js')?>"></script>

<!---->
<script type="text/javascript" src="<?=base_url('assets/datatable/js/dataTables.buttons.min.js')?>"></script>

<!---->
<script type="text/javascript" src="<?=base_url('assets/datatable/js/jszip.min.js')?>"></script>

<!---->
<script type="text/javascript" src="<?=base_url('assets/datatable/js/buttons.html5.min.js')?>"></script>

<!---->
<script type="text/javascript" src="<?=base_url('assets/datatable/js/dataTables.bootstrap.min.js')?>"></script>

<!---->
<script type="text/javascript" src="<?=base_url('assets/js/d3.js')?>"></script>

<!---->
<script type="text/javascript" src="<?=base_url('assets/js/crossfilter.js')?>"></script>

<!---->
<script type="text/javascript" src="<?=base_url('assets/js/dc.js')?>"></script>

<!---->
<script type="text/javascript" src="<?=base_url('assets/js/FileSaver.js')?>"></script>

<!---->
<script type="text/javascript" src="<?=base_url('assets/js/lysenko-interval-tree.js')?>"></script>

<!---->
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>

<!---->
<script src="//cdn.datatables.net/plug-ins/1.10.7/sorting/datetime-moment.js"></script>

<!--<script type="text/javascript" src="<?=base_url('assets/js/resultados.js')?>"></script>-->

<script src="https://d3js.org/d3-time.v1.min.js"></script>
<script src="https://d3js.org/d3-time-format.v2.min.js"></script>

<!---->
<!-- Este codigo javascript genera las tablas-->
<script >
    
	$('#data-table').DataTable({
        "columns": [
            { "data": "Curso" },
            { "data": "Alumno" },
			{ "data": "Actividad" },
            { "data": "Ejercicio" },
            { "data": "Resultado" },
            { "data": "Fecha" }
        ],
        "columnDefs": [
            {
                "type": "date",
                "targets": 5,
                "dateFormat": "yy-mm-dd",
                "render": function (data) {
                    var date = new Date(data), month = date.getMonth() + 1, day = date.getDate();
                    return date.getFullYear() + "-" + (month.length > 1 ? month : "0" + month) + "-" + (day > 9 ? day : "0" + day);
                }
            },
            {
                "targets": 4,
                "render": function (data) {
                    if (data[0] > 0) {
						return "<a href=\"javascript:void(0)\" onClick=\"nombre_funcion('mensaje_" + data.split('#')[4] + "')\" ><i class=\"fa fa-check text-success\" data-toggle=\"tooltip\" title=\"Correcto\"><p style=\"display:none\">0</p></i></a><div id=\"mensaje_" + data.split('#')[4] + "\" style=\"display:none\"><p><h4>" + data.split('#')[1] + "</h4><p> <p><b>Respuesta: &nbsp </b>" + data.split('#')[2] + "</p><b> &nbsp Intentos:</b> &nbsp " + data.split('#')[3] + "</div>";}
                    return '<i class="fa fa-times text-danger" data-toggle="tooltip" title="Incorrecto"><p style="display:none">1</p></i>'
                }
				
				},
            
			{
                "targets": 3,
                "render": function (data) {
                    return '<span data-toggle="tooltip" data-placement="top" title="' + data.split('#')[1] + '" >' + data.split('#')[0] +'</span>';
                }
            },
			
			{
                "targets": 2,
                "render": function (data) {
                    return '<span data-toggle="tooltip" data-placement="top" title="' + data.split('#')[1] + '" >' + data.split('#')[0] +'</span>';
                }
            },
            {
                "targets": 1,
                "render": function (data) {
                    return '<a href="resultados?alumno_id=' + data.split('#')[0] + '" >' + data.split('#')[1] +'</a>'
                }
            },
            {
                "targets": 0,
                "render": function (data) {
                    return '<a href="resultados?curso_id=' + data.split('#')[0] + '" >' + data.split('#')[1] +'</a>'
                }
            }
        ],
        "language": {
            "lengthMenu": "<strong>Mostrar</strong> _MENU_ ",
            "info":"Mostrando _START_ hasta _END_ de _TOTAL_ ",
            "search":"<strong>Filtrar </strong> _INPUT_",
            "infoFiltered":"(filtrado de _MAX_ )",
            "zeroRecords": "Sin resultados ",
            "paginate": {
                "previous":"Anterior",
                "next":"Siguiente"
            }
        }
    });
</script>

<!-- Este codigo javascript genera los graficos-->
<script>
    /*Variables representacion de los graficos*/
	var aplicacionesChart = dc.pieChart("#chart-applications");
    var respuestasChart = dc.pieChart("#chart-respuestas");
    var actividadesChart = dc.rowChart("#chart-actividades");
    var ejerciciosChart = dc.rowChart("#chart-ejercicios");
    var monthChart = dc.barChart("#chart-resultados");
	var filtroChart = dc.barChart("#chart-filtro");
	
	/*Variable que identifica el tipo de usuario*/
	var query = document.URL.split('?'), ndx;

	/*Si se encuentran datos se realizan las operaciones*/
    if (query.length > 1) {
		
        d3.json("/plataforma/index.php/resultados/json?" + query[1], function (error, data) {
    
			/*Si no hay datos, se informa.*/
            if (data.length < 1) {
                $("#chart-applications, #chart-respuestas, #chart-actividades, #chart-ejercicios, #chart-resultados, #data-table_wrapper").hide();
                $("#chart-alerta").show();
                $('.progress').fadeOut();
                return
            }
			/*
			* Modificacion de titulo de graficos dependiendo del usuario.
			*/
            $(document).ready(function() {
                if (query[1].split('=')[0] == 'institucion_id') {
                    $('#chart-resultados h1').append('<small> Institucion: ' + data[0].institucion_nombre + '</small>');
                    $('.breadcrumb li:last').text('Resultados: ' + data[0].institucion_nombre);
                    document.title = 'Resultados - Institucion: ' + data[0].institucion_nombre + ' | intAR21'
                }

                if (query[1].split('=')[0] == 'grupo_id') {
                    $('#chart-resultados h1').append('<small> Grupo: ' + data[0].grupo_identificador + '</small>');
                    $('.breadcrumb li:last').text('Resultados: ' + data[0].grupo_identificador);
                    document.title = 'Resultados - Grupo: ' + data[0].grupo_identificador + ' | intAR21'
                }

                if (query[1].split('=')[0] == 'alumno_id') {
                    $('#chart-resultados h1').append('<small> Alumno: ' + data[0].alumno_nombres + '</small>');
                    $('.breadcrumb li:last').text('Resultados: ' + data[0].alumno_nombres);
                    document.title = 'Resultados - Alumno: ' + data[0].alumno_nombres + ' | intAR21'
                }

                if (query[1].split('=')[0] == 'curso_id') {
                    $('#chart-resultados h1').append('<small> Curso: ' + data[0].curso_grado + data[0].curso_letra + '</small>');
                    $('.breadcrumb li:last').text('Resultados: ' + data[0].curso_grado + data[0].curso_letra );
                    document.title = 'Resultados - Curso: ' + data[0].curso_grado + data[0].curso_letra + ' | intAR21'
                }

                if (query[1].split('=')[0] == 'usuario_id') {
                    $('#chart-resultados h1').append('<small> Usuario: ' + data[0].usuario_nombre + '</small>');
                    $('.breadcrumb li:last').text('Resultados: ' + data[0].usuario_nombre);
                    document.title = 'Resultados - Usuario: ' + data[0].usuario_nombre + ' | intAR21'
                }

                if (query[1].split('=')[0] == 'actividad_id') {
                    $('#chart-resultados h1').append('<small> Actividad: ' + data[0].actividad_texto + '</small>');
                    $('.breadcrumb li:last').text('Resultados: ' + data[0].actividad_texto);
                    document.title = 'Resultados - Actividad: ' + data[0].actividad_texto + ' | intAR21'
                }
            });
			
			
			/*
			* intervalo de datos.
			*/
            data.forEach(function(e) {
                e.interval = new Date(e.resultado_fechahora.split('/')[2], parseInt(e.resultado_fechahora.split('/')[1]) - 1, e.resultado_fechahora.split('/')[0]);
            });

			/*filtro de datos*/
            ndx = crossfilter(data);
			ndx.groupAll();
			
			/*dimenciones*/
            var respuestaDim = ndx.dimension(function(e) { if (e.resultado_correcta == 1) { return 'Correctas' } else { return 'Incorrectas' } });
            var ejercicioDim = ndx.dimension(function(e) { return 'Ejercicio ' + e.ejercicio_numero });
            var actividadDim = ndx.dimension(function(e) { return 'Actividad' + e.actividad_numero });
            var aplicacionDim = ndx.dimension(function(e) { return e.aplicacion_nombre });
            var fechasDim = ndx.dimension(function(e) { return e.interval; });
			var fechasDimF = ndx.dimension(function(e){ return e.interval; });
			
			/*grupos*/
            var correctaPerResult = respuestaDim.group().reduceSum(function(){ return 1 }),
                correctaPorEjercicio = ejercicioDim.group().reduceSum(function(){ return 1 }),
                correctaPorActividad = actividadDim.group().reduceSum(function() { return 1 }),
                correctaPorAplicacion = aplicacionDim.group().reduceCount(),
                correctaPorFecha = fechasDim.group().reduceCount();
				correctaPorFechaF = fechasDimF.group().reduceCount();

            var max = new Date(d3.max(correctaPorFecha.all(), function (e) { return e.key })),
                min = new Date(d3.min(correctaPorFecha.all(), function (e) { return e.key }));
			
            console.log(fechasDim);
			/*grafico de barras, muestra la cantidadd de ejercicios relizados por fechas fechas*/
			monthChart
                .dimension(fechasDim)
                .group(correctaPorFecha)
				/*.chart(function(c) { return dc.lineChart(c).interpolate('cardinal').evadeDomainFilter(true); })*/
                .x(d3.time.scale().domain([min.setDate(min.getDate() - 7), max.setDate(max.getDate() + 7)]))
                .elasticY(true)
				.mouseZoomable(true)
				.rangeChart(filtroChart)
                .controlsUseVisibility(true)
                .height(250)
                .centerBar(true)
                .xUnits(d3.time.days)
                .alwaysUseRounding(true)
                .gap(1);
			
			filtroChart

				.dimension(fechasDimF)
                .group(correctaPorFechaF)
                .x(d3.time.scale().domain([min.setDate(min.getDate() - 7), max.setDate(max.getDate() + 7)]))
                //.elasticY(true)
                .controlsUseVisibility(true)
                .height(60)
                .centerBar(true)
				//.yAxis().ticks(5);
                .xUnits(d3.time.years)
                .gap(1)
				.alwaysUseRounding(true)
                .yAxis().ticks(0).tickFormat(function(){});
			
			/*grfico torta que muetra las aplicaciones que son usadas*/
            aplicacionesChart
                .dimension(aplicacionDim)
                .group(correctaPorAplicacion)
                .controlsUseVisibility(false)
                .height(300)
                .width(240)
                .cx(120)
                .cy(180)
                .legend(dc.legend())
                .on('pretransition', function(chart) {
                    updatedData();

                    chart
                        .selectAll('text.pie-slice')
                        .text(function(d) {
                            return /*d.data.key + ': ' +*/ dc.utils.printSingleValue((d.endAngle - d.startAngle) / (2*Math.PI) * 100) + '%';
                        })
                });

			/*grafico de barras que muestra la cantidad de ejercicios*/
            respuestasChart
                .dimension(respuestaDim)
                .group(correctaPerResult)
                .controlsUseVisibility(true)
                .height(300)
                .width(240)
                .cx(120)
                .cy(180)
                .legend(dc.legend())
                .on('pretransition', function(chart) {
                    chart
                        .selectAll('text.pie-slice')
                        .text(function(d) {
                            return ((d.endAngle - d.startAngle)/ (2 * Math.PI) * 100).toFixed(2) + ' %'
                        })
                });
			/*grafico que muestra las actividades*/
            actividadesChart
                .dimension(actividadDim)
                .group(correctaPorActividad)
                .elasticX(true)
                .controlsUseVisibility(true)
                .height(350);
			/*grafico que muestra los ejercicios realizados*/
            ejerciciosChart
                .dimension(ejercicioDim)
                .group(correctaPorEjercicio)
                .elasticX(true)
                .controlsUseVisibility(true)
                .height(350);

            dc.renderAll();



            function updatedData(){
                var datos = respuestaDim.top(Infinity);

                $('#data-table')
                    .DataTable()
                    .clear();

                datos.forEach(function(e) {
                    var element = new Object();

                    element['Curso'] = e.curso_id + '#' + e.curso_grado + e.curso_letra;
                    element['Alumno'] = e.alumno_id + '#' + e.alumno_nombres;
                    element['Ejercicio'] = e.ejercicio_numero  + '#'+ e.ejercicio_nombre + '#' + e.resultado_id + 'A';
                    element['Resultado'] = e.resultado_correcta + '#' + e.resultado_pregunta + '#' + e.resultado_seleccion + '#' + e.resultado_oportunidad + '#' + e.resultado_id;
					element['Actividad'] = e.actividad_nombre + '#' +e.actividad_descripcion ;

                    var parts =e.resultado_fechahora.split('/');

                    element['Fecha'] = new Date(parts[2], parts[1]-1, parts[0]);


                    $('#data-table')
                        .DataTable()
                        .row
                        .add(element)

                });

                $('#data-table')
                    .DataTable()
                    .draw();

                $('.progress').fadeOut()
            }
        })
    }
	
	function actualizar(){
		//dc.barChart("#chart-resultados").zoomScale([$("#fecharesultadosinicio").datepicker({maxDate: "0d"}),$("#fecharesultadosfinal").datepicker({maxDate: "0d"})]);
		//dc.renderAll();
	}
</script>
