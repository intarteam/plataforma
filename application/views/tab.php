<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <?php
      foreach ($tabs as $tab) {
        echo '<li role="presentation"><a href="#'.$tab.'" aria-controls="'.$tab.'" role="tab" data-toggle="tab">'.$tab.'</a></li>';
      }
    ?>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <?php
      foreach ($tabs as $tab) {
        echo '<div role="tabpanel" class="tab-pane" id="'.$tab.'">';
        $this->load->view($tab);
        echo '</div>';
      }
    ?>
  </div>

</div>
