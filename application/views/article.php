<style>
  .welcome-header {
    background-image: url('/plataforma/assets/images/plataforma no effect.png');
    background-position: 50% 50%;
    background-size: cover;
    margin: -20px -30px;
    min-height: 60%;
    text-align: right;
    padding: 10px 30px;
  }
  
  .welcome-header h1 {
    color: #fff;
    font-size: 50pt;
  }
  
  .panel-primary > .panel-heading {
    background-color: #0085ff;
    border-color: #0085ff;
  }
  
  .panel-green > .panel-heading {
    background-color: #009aa3;
    color: #fff;
  }
  
  .panel-green {
    border-color: #009aa3;
  }
  
  .panel-yellow > .panel-heading {
    background-color: #a59e6a;
    color: #fff;
  }
  
  .panel-yellow {
    border-color: #a59e6a;
  }

  .panel-red > .panel-heading {
    background-color: #ab5c62;
    color: #fff;
  }
  
  .panel-red {
    border-color: #ab5c62;
  }
  
  .huge {
    font-size: 20pt;
  }
  .img-thumbnail {
    margin-bottom: 10px;
  }
</style>

<div class="welcome-header">
  <h1>Ayuda</h1>
</div>

<br><br>

<div class="row">
  <div class="col-sm-4">
      <div class="panel panel-primary">
          <div class="panel-heading">
              <div class="row">
                  <div class="col-xs-3">
                      <i class="fa fa-question fa-5x"></i>
                  </div>
                  <div class="col-xs-9 text-right">
                      <div class="huge">Preguntas frecuentes</div>
                      <div></div>
                  </div>
              </div>
          </div>
          <a href="#">
              <div class="panel-footer">
                  <span class="pull-left">Revisar preguntas</span>
                  <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                  <div class="clearfix"></div>
              </div>
          </a>
      </div>
  </div>
  <div class="col-sm-4">
      <div class="panel panel-green">
          <div class="panel-heading">
              <div class="row">
                  <div class="col-xs-3">
                      <i class="fa fa-youtube-play fa-5x"></i>
                  </div>
                  <div class="col-xs-9 text-right">
                      <div class="huge">Video tutoriales</div>
                      <div></div>
                  </div>
              </div>
          </div>
          <a href="#" data-toggle="modal" data-target="#myModal">
              <div class="panel-footer">
                  <span class="pull-left">Ver videos</span>
                  <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                  <div class="clearfix"></div>
              </div>
          </a>
      </div>
  </div>
  <div class="col-sm-4">
      <div class="panel panel-yellow">
          <div class="panel-heading">
              <div class="row">
                  <div class="col-xs-3">
                      <i class="fa fa-book fa-5x"></i>
                  </div>
                  <div class="col-xs-9 text-right">
                      <div class="huge">Manual</div>
                      <div></div>
                  </div>
              </div>
          </div>
          <a href="#manual">
              <div class="panel-footer">
                  <span class="pull-left">Leer manual</span>
                  <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                  <div class="clearfix"></div>
              </div>
          </a>
          <a href="#">
              <div class="panel-footer">
                  <span class="pull-left">Descargar manual en formato PDF</span>
                  <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                  <div class="clearfix"></div>
              </div>
          </a>
      </div>
  </div>
  
  <hr class="col-sm-12"  id="manual">
  
  
  <div>
    <h4>&nbsp;</h4>
    <h1>Manual de uso <small>Sistema de evaluacion</small></h1>
    <p>Esta plataforma tiene su enfoque en el avance de los alumnos, y de su grupo curso, permitiendo así a los docentes visualizar 
      diversas características para lograr lo anterior, tales como gráficos, herramientas de gestión tanto de alumnos como de docentes 
      y cursos. Permite adicionalmente ver las actividades que se realizan por cada aplicación, los objetivos y descripciones de estas.</p>
    
    <h2>1. Acceso <small>a la plataforma</small></h2>
    <p>Para ingresar a la plataforma, se puede hacer accediendo a las página web www.intar21.com y presionar donde dice: Acceso 
      plataforma, o dirigirse al sitio http://www.intar21.com/plataforma, en ambas formas tiene que ingresar con su RUT y contraseña.</p>
    <p>Es necesario mencionar que existen tres tipos de usuarios, los cuales tienen acceso diferido, mostrando características 
      distintas en base al tipo de usuario, y estos son:</p>
    <ul>
      <li>Administrador: Pueden ver la información de todas las instituciones y adicionalmente tienen habilitadas todas las funciones.</li>
      <li>Director: Puede ingresar docentes nuevos, administrarlos y cambiar datos no sensibles de la institución a la que representa.</li>
      <li>Docente: Puede agregar alumnos nuevos, administrarlos y cambiar datos no sensibles de su cuenta.</li>
    </ul>
    <p>Todos los tipos de usuarios tienes funciones en común, como por ejemplo ver el avance de los alumnos, de los cursos y de los 
      grupos PIE.</p>
    
    <h2>2. Pantalla de ingreso</h2>
    <p>Esta página está destinada a realizar el inicio de sesión de los usuarios, pidiendo el <ins>rut</ins> y la <ins>contraseña</ins>. 
      En caso de pertenecer al sistema, pasará automáticamente a la pantalla de bienvenida al Sistema de Evaluacion.</p>
    <img class="img-responsive img-thumbnail" src="/plataforma/assets/images/pan-ingreso.png">
    
    <div class="col-lg-4">
      <h4>Rut</h4>
      <p>Corresponde al Rol Único Tributario del usuario, se debe ingresar sin puntos, con guion e incluyendo el digito verificador.</p>
      <a 
         class="thumbnail" 
         style="background-image:url('/plataforma/assets/images/pan-ingreso.png');
                background-position:50.8% 52.5%;
                height:44px;
                width:300px"></a>
    </div>
    <div class="col-lg-4">
      <h4>Contraseña</h4>
      <p>Corresponde a la contraseña asignada por el sistema, si es que es la primera vez que ingresa al sistema, o por el mismo usuario
      en caso de haber modificado la contraseña.</p>
      <a 
         class="thumbnail" 
         style="background-image:url('/plataforma/assets/images/pan-ingreso.png');
                background-position:50.8% 59.5%;
                height:44px;
                width:300px"></a>
    </div>
    <div class="col-lg-4">
      <h4>Ingresar</h4>
      <p>Todos los tipos de usuarios tienes funciones en común, como por ejemplo ver el avance de los alumnos, de los cursos y de los 
      grupos PIE.</p>
      <a 
         class="thumbnail" 
         style="background-image:url('/plataforma/assets/images/pan-ingreso.png');
                background-position:50.8% 73.5%;
                height:95px;
                width:320px"></a>
    </div>
    
    <h2>3. Pantalla de bienvenida</h2>
    <p>Esta pantalla es la que se verá siempre dentro de la plataforma, dejando así los enlaces a las diversas secciones a un costado. 
    Las funciones de cada enlace se describen a continuación.</p>
    <img class="img-responsive img-thumbnail" src="/plataforma/assets/images/pan-bienvenido.png">
    
    <div class="col-md-3">
      <h4>Cursos</h4>
      <p>Ubicado en la parte superior de la pantalla, indica la ubicacion en que se encuentra en la plataforma.</p>
      <a 
         class="thumbnail" 
         style="background-image:url('/plataforma/assets/images/pan-bienvenido.png');
                background-position:25.3% 90.6%;
                height:160px;
                width:auto;
                max-width:240px"></a>
    </div>
    
    <div class="col-md-3">
      <h4>Grupos</h4>
      <p>Ubicado en la parte superior de la pantalla, indica la ubicacion en que se encuentra en la plataforma.</p>
      <a 
         class="thumbnail" 
         style="background-image:url('/plataforma/assets/images/pan-bienvenido.png');
                background-position:49.8% 90.6%;
                height:160px;
                width:auto;
                max-width:240px"></a>
    </div>
    
    <div class="col-md-3">
      <h4>Alumnos</h4>
      <p>Ubicado en la parte superior de la pantalla, indica la ubicacion en que se encuentra en la plataforma.</p>
      <a 
         class="thumbnail" 
         style="background-image:url('/plataforma/assets/images/pan-bienvenido.png');
                background-position:74.3% 90.6%;
                height:160px;
                width:auto;
                max-width:240px"></a>
    </div>
    
    <div class="col-md-3">
      <h4>Actividades</h4>
      <p>Ubicado en la parte superior de la pantalla, indica la ubicacion en que se encuentra en la plataforma.</p>
      <a 
         class="thumbnail" 
         style="background-image:url('/plataforma/assets/images/pan-bienvenido.png');
                background-position:98.7% 90.6%;
                height:160px;
                width:auto;
                max-width:240px"></a>
    </div>
    
    <div class="col-md-3">
      <h4>Menu</h4>
      <p>Ubicado en la esquina superior izquierda de la pantalla, este boton le permite disminuir/aumentar el tamaño del menu de 
        navegacion</p>
      <a 
         class="thumbnail" 
         style="background-image:url('/plataforma/assets/images/pan-bienvenido.png');
                background-position:0.5% 0%;
                height:42px;
                width:auto;
                max-width:240px"></a>
    </div>
    
    <div class="col-md-3">
      <h4>Barra de navegacion</h4>
      <p>Ubicado en la parte superior de la pantalla, indica la ubicacion en que se encuentra en la plataforma.</p>
      <a 
         class="thumbnail" 
         style="background-image:url('/plataforma/assets/images/pan-bienvenido.png');
                background-position:22.5% 0%;
                height:42px;
                width:auto;
                max-width:240px"></a>
    </div>
    
    <div class="col-md-12">
      <h2>4. Menu lateral</h2>
      <br>
    </div>

    <div class="col-md-3">
      <img class="img-responsive img-thumbnail" src="/plataforma/assets/images/pan-sidebar.png">
    </div>

    <div class="col-md-3">
      <h4>Imagen de la institucion</h4>
      <p>Ubicado en la parte superior de la pantalla, indica la ubicacion en que se encuentra en la plataforma.</p>
      <a 
         class="thumbnail" 
         style="background-image:url('/plataforma/assets/images/pan-bienvenido.png');
                background-position:11% 21%;
                height:70px;
                width:auto;
                max-width:70px"></a>
    </div>
    
    <div class="col-md-3">
      <h4>Nombre del docente</h4>
      <p>Ubicado en la parte inferior a la "Imagen del docente", indica el nombre del usuario con que se ha iniciado la sesion.</p>
      <a 
         class="thumbnail" 
         style="background-image:url('/plataforma/assets/images/pan-bienvenido.png');
                background-position:2% 33.4%;
                height:50px;
                width:auto;
                max-width:220px"></a>
    </div>
    
    <div class="col-md-3">
      <h4>Imagen del docente</h4>
      <p>Ubicado en la parte superior de la pantalla, indica la ubicacion en que se encuentra en la plataforma.</p>
      <a 
         class="thumbnail" 
         style="background-image:url('/plataforma/assets/images/pan-bienvenido.png');
                background-position:4% 12%;
                height:140px;
                width:auto;
                max-width:140px"></a>
    </div>
    
    <div class="col-md-3">
      <h4>Pestaña "Alumnos"</h4>
      <p>Permite acceder a las opciones de gestion de los alumnos: "Agregar Alumno" y "Gestion de Alumnos".</p>
      <a 
         class="thumbnail" 
         style="background-image:url('/plataforma/assets/images/pan-sidebar.png');
                background-position:2% 49.5%;
                height:42px;
                width:auto;
                max-width:220px"></a>
    </div>
    
    <div class="col-md-3">
      <h4>Pestaña "Cursos"</h4>
      <p>Permite acceder a la pantalla de gestion de cursos del docente.</p>
      <a 
         class="thumbnail" 
         style="background-image:url('/plataforma/assets/images/pan-sidebar.png');
                background-position:2% 57.3%;
                height:42px;
                width:auto;
                max-width:220px"></a>
    </div>
    
    <div class="col-md-3">
      <h4>Pestaña "Inicio"</h4>
      <p>Ubicado en la parte inferior al "Nombre del docente", permite acceder a la pantalla de inicio de la platforma.</p>
      <a 
         class="thumbnail" 
         style="background-image:url('/plataforma/assets/images/pan-sidebar.png');
                background-position:2% 41.5%;
                height:42px;
                width:auto;
                max-width:220px"></a>
    </div>
    
    <div class="col-md-3">
      <h4>Pestaña "Grupos"</h4>
      <p>Permite acceder a la pantalla de gestion de grupos de la institucion.</p>
      <a 
         class="thumbnail" 
         style="background-image:url('/plataforma/assets/images/pan-sidebar.png');
                background-position:2% 65.3%;
                height:42px;
                width:auto;
                max-width:220px"></a>
    </div>
    
    <div class="col-md-3">
      <h4>Pestaña "Actividades"</h4>
      <p>Permite acceder a la pantalla de informacion de las actividades.</p>
      <a 
         class="thumbnail" 
         style="background-image:url('/plataforma/assets/images/pan-sidebar.png');
                background-position:2% 73%;
                height:42px;
                width:auto;
                max-width:220px"></a>
    </div>
    
    <div class="col-md-3">
      <h4>Cerrar Sesion</h4>
      <p>Boton para cerrar la sesion actual del docente.</p>
      <a 
         class="thumbnail" 
         style="background-image:url('/plataforma/assets/images/pan-sidebar.png');
                background-position:2% 81%;
                height:42px;
                width:auto;
                max-width:220px"></a>
    </div>
  </div>
  
  <div class="col-md-12">
    <h2>5. Alumnos</h2>
  </div>
  
  <div class="col-md-12">
    <h3>5.1. Agregar alumno</h3>
    <p>Permite ingresar manualmente a los alumnos, ingresando sus datos básicos y de contacto. Adicionalmente, esta pantalla 
      contendrá una opción para importar a los alumnos desde un archivo en formato XLS.</p>
    <img class="img-responsive img-thumbnail" src="/plataforma/assets/images/pan-agregaralumno.png">
  </div>
  
  <div class="col-md-3">
    <h4>Nombres</h4>
    <p>Aca debe ingresar el o los nombres del alumno.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-agregaralumno.png');
              background-position:26.2% 28%;
              height:33px;
              width:auto;
              max-width:250px"></a>
  </div>
  
  <div class="col-md-3">
    <h4>Apellido Paterno</h4>
    <p>Aca debe ingresar el apellido paterno del alumno.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-agregaralumno.png');
              background-position:58.1% 28%;
              height:33px;
              width:auto;
              max-width:250px"></a>
  </div>
  
  <div class="col-md-3">
    <h4>Apellido Materno</h4>
    <p>Aca debe ingresar el apellido materno del alumno.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-agregaralumno.png');
              background-position:90.1% 28%;
              height:33px;
              width:auto;
              max-width:250px"></a>
  </div>
  
  <div class="col-md-3">
    <h4>Rut</h4>
    <p>Aca debe ingresar el RUT (Rol Único Tributario) del alumno.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-agregaralumno.png');
              background-position:26.2% 39.8%;
              height:33px;
              width:auto;
              max-width:250px"></a>
  </div>
  
  <div class="col-md-3">
    <h4>Codigo</h4>
    <p>Corresponde a un codigo ocupado por nuestro sistema para identificar mas rapidamente al alumno. El sistema se encargara de 
      generar el codigo una vez que el alumno sea agregado.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-agregaralumno.png');
              background-position:58.1% 39.8%;
              height:33px;
              width:auto;
              max-width:230px"></a>
  </div>
  
  <div class="col-md-3">
    <h4>Responsable</h4>
    <p>Aca debe ingresar el o los nombres y apellidos del responsable (apoderado) del alumno.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-agregaralumno.png');
              background-position:81.5% 39.8%;
              height:33px;
              width:auto;
              max-width:230px"></a>
  </div>
  
  <div class="col-md-3">
    <h4>Telefono</h4>
    <p>Aca debe ingresar el telefono de contacto del responsable del alumno.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-agregaralumno.png');
              background-position:26.3% 51.8%;
              height:33px;
              width:auto;
              max-width:230px"></a>
  </div>
  
  <div class="col-md-3">
    <h4>Grupo</h4>
    <p>Aca puede asignar si el alumno pertenece a alguno de los Grupos PIE de la institucion o si no pertence a ninguno. </p>
    <p>Se puede seleccionar solo un grupo por alumno.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-agregaralumno.png');
              background-position:58% 51.8%;
              height:33px;
              width:auto;
              max-width:230px"></a>
  </div>
  
  <div class="col-md-3">
    <h4>Institucion</h4>
    <p>Aca se indica la institucion a la cual se agregara el alumno.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-agregaralumno.png');
              background-position:26.5% 72.3%;
              height:33px;
              width:auto;
              max-width:230px"></a>
  </div>
  
  <div class="col-md-3">
    <h4>Comuna</h4>
    <p>Aca debe ingresar la comuna donde vive el alumno.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-agregaralumno.png');
              background-position:50.3% 72.3%;
              height:33px;
              width:auto;
              max-width:230px"></a>
  </div>
  
  <div class="col-md-4">
    <h4>Direccion</h4>
    <p>Aca debe ingresar la direccion de contacto del alumno.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-agregaralumno.png');
              background-position:78.5% 72.3%;
              height:33px;
              width:auto;
              max-width:300px"></a>
  </div>
  
  <div class="col-md-4">
    <h4>Boton "Agregar Alumno"</h4>
    <p>Este boton agregara un nuevo alumno en el sistema con lso datos anteriormente ingresados.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-agregaralumno.png');
              background-position:98% 80.3%;
              height:33px;
              width:auto;
              max-width:130px"></a>
  </div>
  
  <div class="col-md-12">
    <h3>5.2. Importar alumno</h3>
    <img class="img-responsive img-thumbnail" src="/plataforma/assets/images/pan-importaralumnos.png">
  </div>
  
  <div class="col-md-4">
    <h4>Institucion</h4>
    <p>Indica la institucion a la cual los alumnos seran agregados.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-importaralumnos.png');
              background-position:26% 27.4%;
              height:33px;
              width:auto;
              max-width:200px"></a>
  </div>
  
  <div class="col-md-4">
    <h4>Seleccionar archivo</h4>
    <p>Selecciona el archivo excel con los datos de los alumnos.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-importaralumnos.png');
              background-position:53% 27.4%;
              height:33px;
              width:auto;
              max-width:300px"></a>
  </div>
  
  <div class="col-md-4">
    <h4>Boton "Agregar alumnos"</h4>
    <p>Agregara los alumnos con los datos mostrados en la tabla, la cual se ha cargado desde el archivo excel.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-importaralumnos.png');
              background-position:82% 27.4%;
              height:33px;
              width:auto;
              max-width:135px"></a>
  </div>
  
  <div class="col-md-12">
    <h3>5.3. Gestion de alumnos</h3>
    <p>Aquí se podrá visualizar y administrar a los alumnos, ver información adicional y filtrarlos por curso (solo se mostrarán 
      a los alumnos del año en curso). Al presionar los iconos del costado (ambas lupas) se mostrarán los diálogos que se ven a 
      continuación. </p>
    <img class="img-responsive img-thumbnail" src="/plataforma/assets/images/pan-gestionalumnos.png">
  </div>
  
  <div class="col-md-3">
    <h4>Boton "Agregar alumno"</h4>
    <p>Este boton lo enviara a la pantalla "Agregar alumno", detallada anteriormente.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-gestionalumnos.png');
              background-position:84% 10.6%;
              height:33px;
              width:auto;
              max-width:150px"></a>
  </div>
  
  <div class="col-md-3">
    <h4>Boton "Descargar en Excel"</h4>
    <p>Este boton descargara un archivo excel con los alumnos cargados en la tabla "Gestion de Alumnos".</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-gestionalumnos.png');
              background-position:98.5% 10.6%;
              height:33px;
              width:auto;
              max-width:160px"></a>
  </div>
  
  <div class="col-md-3">
    <h4>Seleccion "Mostrar"</h4>
    <p>Este selector permite controlar la cantidad de alumnos a mostrar por pagina.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-gestionalumnos.png');
              background-position:22.5% 23%;
              height:40px;
              width:auto;
              max-width:135px"></a>
  </div>
  
  <div class="col-md-3">
    <h4>Seleccion "Curso"</h4>
    <p>Este selector permite filtar los alumnos por el curso al cual pertencen.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-gestionalumnos.png');
              background-position:32.5% 23%;
              height:40px;
              width:auto;
              max-width:115px"></a>
  </div>
  
  <div class="col-md-3">
    <h4>Seleccion "Comuna"</h4>
    <p>Este selector permite filtar los alumnos por la comuna en la que viven.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-gestionalumnos.png');
              background-position:43.5% 23%;
              height:40px;
              width:auto;
              max-width:175px"></a>
  </div>
  
  <div class="col-md-3">
    <h4>Filtar</h4>
    <p>Este buscador te permite filtrar los resultados por el texto especifico que escribas.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-gestionalumnos.png');
              background-position:99.2% 23%;
              height:40px;
              width:auto;
              max-width:218px"></a>
  </div>
  
  <div class="col-md-3">
    <h4>Cabecera "Rut"</h4>
    <p>Esta cabecera permite ordenar los alumnos por Rut, de manera ascendente y descendente.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-gestionalumnos.png');
              background-position:22.6% 32%;
              height:42px;
              width:auto;
              max-width:100px"></a>
  </div>
  
  <div class="col-md-3">
    <h4>Cabecera "Nombres"</h4>
    <p>Esta cabecera permite ordenar los alumnos por sus Nombres, de manera ascendente y descendente.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-gestionalumnos.png');
              background-position:33.3% 32%;
              height:42px;
              width:auto;
              max-width:200px"></a>
  </div>
  
  <div class="col-md-3">
    <h4>Cabecera "Apellido Paterno"</h4>
    <p>Esta cabecera permite ordenar los alumnos por su Apellido Paterno, de manera ascendente y descendente.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-gestionalumnos.png');
              background-position:52.8% 32%;
              height:42px;
              width:auto;
              max-width:250px"></a>
  </div>
  
  <div class="col-md-3">
    <h4>Cabecera "Edad"</h4>
    <p>Esta cabecera permite ordenar los alumnos por su Apellido Paterno, de manera ascendente y descendente.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-gestionalumnos.png');
              background-position:70% 32%;
              height:42px;
              width:auto;
              max-width:140px"></a>
  </div>
  
  <div class="col-md-3">
    <h4>Cabecera "Curso"</h4>
    <p>Esta cabecera permite ordenar los alumnos por su Curso, de manera ascendente y descendente.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-gestionalumnos.png');
              background-position:82% 32%;
              height:42px;
              width:auto;
              max-width:140px"></a>
  </div>
  
  <div class="col-md-3">
    <h4>Boton "Editar datos"</h4>
    <p>Permite acceder a la pantalla de edicion de los datos del alumno y asignacion de condiciones.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-gestionalumnos.png');
              background-position:93% 38%;
              height:42px;
              width:auto;
              max-width:60px"></a>
  </div>
  
  <div class="col-md-3">
    <h4>Boton "Ver resultados"</h4>
    <p>Permite acceder a la pantalla de resumen de los resultados del alumno.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-gestionalumnos.png');
              background-position:96% 38%;
              height:42px;
              width:auto;
              max-width:60px"></a>
  </div>
  
  <div class="col-md-3">
    <h4>Boton "Ver reporte"</h4>
    <p>Permite acceder a la pantalla de reporte del alumno.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-gestionalumnos.png');
              background-position:99% 38%;
              height:42px;
              width:auto;
              max-width:60px"></a>
  </div>
    
  <div class="col-md-12">
    <h3>5.4. Resultados de alumno</h3>
    <img class="img-responsive img-thumbnail" src="/plataforma/assets/images/pan-resultadosalumno.png">
  </div>
  
  <div class="col-md-12">
    <h4>Grafico "Cantidad de respuestas en el tiempo"</h4>
    <p>Permite filtrar los resultados en un lapso de tiempo personalizado</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-resultadosalumno.png');
              background-position:85% 10%;
              height:160px;
              width:auto;
              max-width:1046px"></a>
  </div>
  
  <div class="col-md-4">
    <h4>Grafico "Porcentaje de respuestas por aplicacion"</h4>
    <p>Permite filtrar los resultados por aplicacion, ademas de graficar el porcentaje de respuestas.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-resultadosalumno.png');
              background-position:25.4% 32%;
              height:370px;
              width:auto;
              max-width:290px"></a>
  </div>
  
  <div class="col-md-4">
    <h4>Grafico "Cantidad de respuestas por actividad"</h4>
    <p>Permite filtrar los resultados por actividad, ademas de graficar la cantidad de respuestas.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-resultadosalumno.png');
              background-position:50% 33.5%;
              height:410px;
              width:auto;
              max-width:280px"></a>
  </div>
  
  <div class="col-md-4">
    <h4>Grafico "Cantidad de respuestas por ejercicio"</h4>
    <p>Permite filtrar los resultados por ejercicio, ademas de graficar la cantidad de respuestas.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-resultadosalumno.png');
              background-position:73% 33.5%;
              height:410px;
              width:auto;
              max-width:280px"></a>
  </div>
  
  <div class="col-md-4">
    <h4>Grafico "Porcentaje de respuestas correctas e incorrectas"</h4>
    <p>Permite filtrar los resultados por respuestas correctas y/o incorrectas, ademas de graficar la cantidad de respuestas.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-resultadosalumno.png');
              background-position:99% 32.5%;
              height:400px;
              width:auto;
              max-width:280px"></a>
  </div>
  
  <div class="col-md-8">
    <h4>Tabla de respuestas</h4>
    <p>Muestra el detalle de cada una de las respuestas del alumno.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-resultadosalumno.png');
              background-position:35% 82%;
              height:400px;
              width:auto;
              max-width:600px"></a>
  </div>
    
  <div class="col-md-12">
    <h3>5.5. Reporte de alumno</h3>
    <p>Corresponde a un resumen del rendimiento obtenido por el alumno en las distintas actividades que el ha realizado, se indica 
      el nombre del usuario que solicita el reporte, el nombre del alumno, su rut, el curso actual al que pertenece, su docente, el 
      porcentaje de respuestas correctas por actividad, el nivel alcanzado y los aspectos que debe reforzar el alumno, en base a los
      porcentajes anteriormente entregados.</p>
    <img class="img-responsive img-thumbnail" src="/plataforma/assets/images/pan-reportealumno.png">
  </div>
  
  <div class="col-md-4">
    <h4>Boton "Imprimir"</h4>
    <p>Permite filtrar los resultados por respuestas correctas y/o incorrectas, ademas de graficar la cantidad de respuestas.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-resultadosalumno.png');
              background-position:99% 32.5%;
              height:400px;
              width:auto;
              max-width:280px"></a>
  </div>
  
  <div class="col-md-4">
    <h4>Boton "Descargar"</h4>
    <p>Permite filtrar los resultados por respuestas correctas y/o incorrectas, ademas de graficar la cantidad de respuestas.</p>
    <a 
       class="thumbnail" 
       style="background-image:url('/plataforma/assets/images/pan-resultadosalumno.png');
              background-position:99% 32.5%;
              height:400px;
              width:auto;
              max-width:280px"></a>
  </div>
  
  <div class="col-md-12">
    <h2>6. Cursos</h2>
  </div>
  
  <div class="col-md-12">
    <h3>6.1. Gestion de cursos</h3>
    <img class="img-responsive img-thumbnail" src="/plataforma/assets/images/pan-gestioncursos.png">
  </div>
  
  <div class="col-md-12">
    <h3>6.2. Agregar curso</h3>
    <p>Esta pantalla muestra un listado de todos los alumnos (por defecto muestra los del año en curso), permitiendo realizar 
      diversas acciones, y junto a ello mostrando diversas opciones para realizar un filtro y buscar así alumnos en específico.</p>
    <img class="img-responsive img-thumbnail" src="/plataforma/assets/images/pan-agregarcurso.png">
  </div>
  
  <div class="col-md-12">
    <h3>6.3. Editar curso</h3>
    <img class="img-responsive img-thumbnail" src="/plataforma/assets/images/pan-editarcurso.png">
  </div>
  
  <div class="col-md-12">
    <h3>6.3. Agregar alumnos al curso</h3>
    <img class="img-responsive img-thumbnail" src="/plataforma/assets/images/pan-agregaralumnos.png">
  </div>
  
  <div class="col-md-12">
    <h3>6.4. Observacion del alumno</h3>
    <img class="img-responsive img-thumbnail" src="/plataforma/assets/images/pan-observacionalumno.png">
  </div>
  
  <div class="col-md-12">
    <h3>6.4. Retirar del alumno</h3>
    <img class="img-responsive img-thumbnail" src="/plataforma/assets/images/pan-retiraralumno.png">
  </div>
  
  <div class="col-md-12">
    <h2>7. Grupos</h2>
  </div>
  
  <div class="col-md-12">
    <h3>7.1. Gestion de grupos</h3>
    <img class="img-responsive img-thumbnail" src="/plataforma/assets/images/pan-grupos.png">
  </div>
  
  <div class="col-md-12">
    <h3>7.2. Agregar grupo</h3>
    <img class="img-responsive img-thumbnail" src="/plataforma/assets/images/pan-agregargrupo.png">
  </div>
  
  <div class="col-md-12">
    <h3>7.3. Editar grupo</h3>
    <img class="img-responsive img-thumbnail" src="/plataforma/assets/images/pan-editargrupo.png">
  </div>
  
  <div class="col-md-12">
    <h3>8. Actividades</h3>
    <p>En esta pantalla se pueden visualizar las diversas actividades que posee cada aplicación, permitiendo además ver un 
      gráfico y su descripción. Al presionar sobre una de las aplicaciones, las actividades mostradas cambiarán sin tener que cargar 
      la página completa. Además, si se presiona el título de la actividad, esta llevará a una pantalla que muestra el listado de los 
      alumnos que han realizado esta actividad y el resultado que obtuvo el alumno (Imagen 1). Junto a lo anterior, el usuario puede ver 
      el listado de los ejercicios realizados en cada actividad presionando el botón de la derecha (donde aparece un lápiz). Al presionar 
      el filtro de cursos, permite ver un gráfico comparativo del curso y el listado de alumnos dentro del curso que realizaron esta 
      actividad y su puntaje (aun no implementado, imagen 2). Si se presiona el botón de filtro por alumno, este muestra un listado de 
      los alumnos que realizaron esta actividad, permitiendo ver un gráfico con cada ejercicio que haya realizado dentro de la actividad 
      (aun sin implementar, imagen 3).</p>
    <img class="img-responsive img-thumbnail" src="/plataforma/assets/images/pan-gestionactividades.png">
  </div>
  
  <div class="col-md-12">
    <h2>9. Cuenta</h2>
  </div>
  
  <div class="col-md-12">
    <h3>9.1. Editar mis datos</h3>
    <p>Permite cambiar los datos no sensibles del mismo, actualizándolos y dejándolos actualizados en el sistema.</p>
    <img class="img-responsive img-thumbnail" src="/plataforma/assets/images/pan-misdatos.png">
  </div>
  
  <div class="col-md-12">
    <h3>9.2. Ver datos de mi institucion</h3>
    <p>Permite al encargado modificar y actualizar en el sistema datos no sensibles de la institución, a su vez 
      permite visualizar los datos del convenio que se posee.</p>
    <img class="img-responsive img-thumbnail" src="/plataforma/assets/images/pan-verdatosinstitucion.png">
  </div>
  
  <div class="col-md-12">
    <h3>9.3. Detalles del contrato</h3>
    <img class="img-responsive img-thumbnail" src="/plataforma/assets/images/pan-detallecontrato.png">
  </div>
  
</div>

<style>
.bigModal{
 width:720px;
}
  .modal-body {
    padding: 0px;
  }
  .close {
    color: red;
  }
</style>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content bigModal">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">intAR21</h4>
      </div>
      <div class="modal-body">
        <div class="embed-responsive embed-responsive-16by9">
          <iframe id="yvideo" width="560" height="315" src="http://www.youtube.com/embed/oFEHSVDDMWo" frameborder="0" allowfullscreen></iframe>
        </div>
      </div>
      <div class="modal-footer">
        <div class="btn-group" role="group" aria-label="Default button group">
          <button id="intar" type="button" class="btn btn-default">intAR21</button>
          <button id="ques" type="button" class="btn btn-default">¿Qué es intAR21?</button>
          <button id="ninos" type="button" class="btn btn-default">Niños utilizando intAR21</button>
          <button id="tecno" type="button" class="btn btn-default">Tecnologías Educativas Inclusivas</button>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {    
    $('.progress').fadeOut()
    
    $('#intar, .close').click(function() {
      $('.modal-title').text('intAR21')
      document.getElementById('yvideo').src = "http://www.youtube.com/embed/oFEHSVDDMWo"
      $('button.active').removeClass('active')
      $(this).toggleClass('active')
    })
    
    $('#ques').click(function() {
      $('.modal-title').text('¿Qué es intAR21?')
      document.getElementById('yvideo').src = "http://www.youtube.com/embed/PwU1ZZ0jBjo"
      $('button.active').removeClass('active')
      $(this).toggleClass('active')
    })
    
    $('#ninos').click(function() {
      $('.modal-title').text('Niños utilizando intAR21')
      document.getElementById('yvideo').src = "http://www.youtube.com/embed/wI9iF65sDLM"
      $('button.active').removeClass('active')
      $(this).toggleClass('active')
    })
    
    $('#tecno').click(function() {
      $('.modal-title').text('Tecnologías Educativas Inclusivas')
      document.getElementById('yvideo').src = "http://www.youtube.com/embed/TauKvLCV2kM"
      $('button.active').removeClass('active')
      $(this).toggleClass('active')
    })
  })
</script>