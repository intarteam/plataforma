<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="/plataforma/assets/images/favicon.png" type="image/x-icon">

    <title>Plataforma | intAR21</title>

    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    
    <!-- Bootstrap core CSS -->
    <link href="<?=base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/css/signin.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/css/button.css')?>" rel="stylesheet">
    
    <link href="<?=base_url("assets/css/jquery.growl.css")?>" rel="stylesheet" type="text/css" />
    
    <!-- jQuery -->
    <script src="<?=base_url('assets/js/jquery.js')?>"></script>
    <!-- Validator -->
    <script type="text/javascript" charset="utf8" src="<?=base_url('assets/js/validator.js')?>"></script>
    <!-- Notify -->
    <script type="text/javascript" charset="utf8" src="<?=base_url('assets/js/jquery.growl.js')?>"></script>
  </head>

  <body>
    <div class="container">
      <form class="form-signin form-row" data-toggle="validator" autocomplete="off" method="post">
        <svg style="margin:0px auto;display:block;" version="1.0" xmlns="http://www.w3.org/2000/svg" width="150pt" height="150pt" viewBox="0 0 400.000000 322.000000" preserveAspectRatio="xMidYMid meet">
          <g transform="translate(0.000000,322.000000) scale(0.100000,-0.100000)" stroke="none">
            <path class="cromosoma" d="M1430 3137 c-112 -25 -215 -114 -274 -237 -29 -60 -31 -73 -31 -170 0 -93 3 -112 27 -162 64 -136 242 -266 500 -362 133 -49 207 -50 270 -3 24 1 53 52 66 79 32 64 22 92 -77 208 -124 145 -165 255 -140 381 15 78 2 126 -53 185 -65 71 -183 104 -288 81z"/>
            <path class="cromosoma" d="M2578 3096 c-26 -7 -62 -24 -80 -38 -43 -33 -91 -123 -111 -208 -65 -272 -88 -336 -160 -447 -88 -136 -199 -236 -333 -300 -74 -35 -224 -75 -424 -112 -349 -66 -685 -239 -932 -482 -264 -260 -371 -542 -312 -826 31 -152 94 -264 209 -371 121 -113 260 -172 407 -172 137 0 250 52 365 169 84 86 148 190 302 497 196 390 311 579 491 802 184 229 296 321 555 457 171 89 228 127 300 200 169 172 234 430 155 613 -42 97 -162 186 -292 217 -74 17 -79 17 -140 1z"/>
            <path class="cromosoma" d="M2447 1916 c-122 -44 -210 -157 -211 -271 0 -69 13 -96 104 -220 66 -89 160 -270 184 -356 8 -30 27 -123 41 -205 33 -191 56 -258 116 -349 115 -172 300 -287 509 -317 206 -30 407 74 495 257 50 104 68 204 62 345 -5 131 -22 203 -73 310 -114 243 -404 477 -930 751 -125 64 -138 69 -196 69 -35 -1 -80 -7 -101 -14z"/>
          </g>
        </svg>
        <h2 class="form-signin-heading" style="text-align:center">Ingrese</h2>
        <label for="usuario_rut" class="sr-only">Rut</label>
        <input type="text" name="usuario_rut" class="form-control" placeholder="Rut: 12345678-9" required autofocus>
        <label for="usuario_clave" class="sr-only">Contraseña</label>
        <input type="password" name="usuario_clave" class="form-control" placeholder="Contraseña" required>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> Recuerdame
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Ingresar</button>
      </form>

    </div> <!-- /container -->
  </body>
  
  <script>
    $(document).ready(function() {
      $('form').validator().on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
          $.post('/plataforma/index.php/ingresar', $('form').serialize(), function() {
             location.reload()
          })
          .fail(function() {
            $.growl.error({ title: "Ouch!", message: "Datos incorrectos, intentelo nuevamente" })
          })
          return false
        }
        $.growl.error({ title: "Hey!", message: "Rellene los datos solicitados" })
      })
    })
  </script>
</html>