<!-- Validator -->
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/js/validator.js')?>"></script>
<!-- Notify -->
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/js/jquery.growl.js')?>"></script>

<style>
  .form-control-feedback {
    right: 15;
  }
  select + .form-control-feedback {
    right: 25;
  }
  form button {
    float: right;
    margin-right: 15;
  }
</style>

<h1><?=$title?></h1>
<hr>
<form class="row" autocomplete="off" method="<?=$metod?>" source="<?=$resrc?>">  
  <div class="form-group has-feedback col-md-4">
    <label class="control-label">Nombre</label>
    <input name="usuario_nombre" type="text" placeholder="Nombre" class="form-control" required>
    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
  </div>
  
  <div class="form-group has-feedback col-md-4">
    <label class="control-label">Apellido Paterno</label>
    <input name="usuario_appat" type="text" placeholder="Apellido Paterno" class="form-control" required>
    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
  </div>
  
  <div class="form-group has-feedback col-md-4">
    <label class="control-label">Apellido Materno</label>
    <input name="usuario_apmat" type="text" placeholder="Apellido Materno" class="form-control" required>
    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
  </div>

  
  <div class="form-group has-feedback col-md-3">
    <label class="control-label">Institución</label>
    <select name="institucion_id" type="text" placeholder="00" class="form-control" required<?php if($this->session->userdata('tipo_id') > 1) { echo ' disabled'; } ?>>
      <?php
        foreach ($this->db->query('select * from institucion')->result() as $institucion)
        {
          if ($this->session->userdata('tipo_id') == 2 and $institucion->institucion_id == $this->session->userdata('iid')) 
          {
            echo '<option value="'.$institucion->institucion_id.'" selected>'.$institucion->institucion_nombre.'</option>';
          }
          else
          {
            echo '<option value="'.$institucion->institucion_id.'">'.$institucion->institucion_nombre.'</option>';
          }
        }
      ?>
    </select>
    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
  </div>
  
  <div class="form-group has-feedback col-md-4">
    <label class="control-label">Correo Electrónico</label>
    <input name="usuario_mail" type="text" placeholder="ejemplo@intar21.com" class="form-control" required>
    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
  </div>
  
  <div class="form-group has-feedback col-md-5">
    <label class="control-label">Imagen</label>
    <input name="usuario_imagen" type="file" placeholder="Apellido Paterno" class="form-control">
    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
  </div>
  
  
  <div class="form-group has-feedback col-md-12">
    <h4>Datos para iniciar sesión</h4>
  </div>
  
  
  <div class="form-group has-feedback col-md-4">
    <label class="control-label">Tipo</label>
    <select name="tipo_id" type="text" placeholder="00" class="form-control" required<?php if($this->session->userdata('tipo_id') > 1) { echo ' disabled'; } ?>>
      <?php
        foreach ($this->db->query('select * from tipo_usuario')->result() as $tipo)
        {
          if ($this->session->userdata('tipo_id') == 2 and $tipo->tipo_id == 3) 
          {
            echo '<option value="'.$tipo->tipo_id.'" selected>'.$tipo->tipo_nombre.'</option>';
          }
          else 
          {
            echo '<option value="'.$tipo->tipo_id.'">'.$tipo->tipo_nombre.'</option>';
          }
        }
      ?>
    </select>
    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
  </div>

  <div class="form-group has-feedback col-md-4">
    <label class="control-label">Rut</label>
    <input name="usuario_rut" type="text" placeholder="12345678-9" class="form-control" required<?php if($this->session->userdata('tipo_id') > 1 and $metod != 'post') { echo ' disabled'; } ?>>
    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
  </div>
  
  <div class="form-group has-feedback col-md-4">
    <label class="control-label">Contraseña</label>
    <input name="usuario_clave" type="password" placeholder="Contraseña" class="form-control"<?php if ($metod != 'patch') { echo ' required'; } ?>>
    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
  </div>
  
  
  <button class="btn btn-primary" type="submit"> Guardar <?php if($this->session->userdata('tipo_id') > 1) { echo 'docente'; } else { echo 'usuario'; } ?></button>
</form>

<script type="text/javascript" charset="utf8" src="<?=base_url('assets/js/editar.js')?>"></script>
