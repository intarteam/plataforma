<!-- Validator -->
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/js/validator.js')?>"></script>
<!-- Notify -->
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/js/jquery.growl.js')?>"></script>

<style>
  .form-control-feedback {
    right: 15;
  }
  form button {
    float: right;
    margin-right: 15;
  }
</style>

<h1><?=$title?></h1>
<hr>
<form class="row" autocomplete="off" method="<?=$metod?>" source="<?=$resrc?>">
  <div class="form-group has-feedback col-md-3">
    <label class="control-label">Aplicación</label>
    <select name="aplicacion_id" type="text" class="form-control" required<?php if($this->session->userdata('tipo_id') > 1) { echo ' disabled'; } ?>>
      <?php
        foreach ($this->db->query('select * from aplicacion')->result() as $aplicacion)
        {
          echo '<option value="'.$aplicacion->aplicacion_id.'">'.$aplicacion->aplicacion_nombre.'</option>';
        }
      ?>
    </select>
    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
  </div>
  
  <div class="form-group has-feedback col-md-7">
    <label class="control-label">Texto</label>
    <input name="actividad_texto" type="text" placeholder="Nombre de la aplicacion" class="form-control" required>
    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
  </div>
  
  <div class="form-group has-feedback col-md-2">
    <label class="control-label">Numero</label>
    <input name="actividad_numero" type="text" placeholder="Nombre de la aplicacion" class="form-control" required>
    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
  </div>
  
  <div class="form-group has-feedback col-md-12">
    <label class="control-label">Nombre</label>
    <textarea name="actividad_nombre" class="form-control" required></textarea>
    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
  </div>
  
  <div class="form-group has-feedback col-md-12">
    <label class="control-label">Objetivo</label>
    <textarea name="actividad_objetivo" class="form-control" required></textarea>
    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
  </div>
  
  <div class="form-group has-feedback col-md-12">
    <label class="control-label">Descripcion</label>
    <textarea name="actividad_descripcion" class="form-control" required></textarea>
    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
  </div>
  
  <button class="btn btn-primary" type="submit"> Guardar actividad</button>
</form>

<script type="text/javascript" charset="utf8" src="<?=base_url('assets/js/editar.js')?>"></script>
