<!-- Validator -->
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/js/validator.js')?>"></script>
<!-- Notify -->
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/js/jquery.growl.js')?>"></script>

<style>
  .form-control-feedback {
    right: 15;
  }
  form button {
    float: right;
    margin-right: 15;
  }
  tbody td:first-child {
    border-left: 1px solid #fff ;
  }
  tbody td:last-child {
    border-right: 1px solid #fff ;
    text-align: right;
  }
  tbody tr:last-child td {
    border-bottom: 1px solid #fff ;
  }
  #datatable tbody td, #datatable_alumnos tbody td {
    border-color: #e7edf5 ;
    padding: 5px 8px;
  }
  #datatable thead th, #datatable_alumnos thead th {
    background-color: #e7edf5;
    border-color: #4a72b2 ;
    color: #4a72b2;
  }
  table .btn {
    padding: 4px 12px;
  }
  table .btn-edit, #datatable .btn-chart, #datatable .btn-report {
    color: #4a72b2;
  }
  table {
    padding: 15px 0px;
  }
  .table-hover>tbody>tr:hover {
    background-color: #eff3f8;
    color: #4a72b2;
  }
  .dataTables_length label {
    padding-right: 8px;
  }
  .dataTables_length select {
    width: auto !important;
  }
  #datatable tbody tr.active>td {
    background-color: #eff3f8 !important;
    color: #4a72b2;
  }
</style>

<h1><?=$title?></h1>
<hr>
<form class="row" autocomplete="off" method="<?=$metod?>" source="<?=$resrc?>">
  <div class="form-group has-feedback col-md-3">
    <label class="control-label">Institución</label>
    <select name="institucion_id" type="text" class="form-control" required<?php if($this->session->userdata('typ') > 2) { echo ' disabled'; } ?>>
      <?php
        foreach ($this->db->query('select * from institucion')->result() as $institucion)
        {
          echo '<option value="'.$institucion->institucion_id.'">'.$institucion->institucion_nombre.'</option>';
        }
      ?>
    </select>
    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
  </div>
  
  <div class="form-group has-feedback col-md-5">
    <label class="control-label">Nombre</label>
    <input name="grupo_identificador" type="text" placeholder="Nombre del grupo" class="form-control" required>
    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
  </div>
  
  <div class="form-group has-feedback col-md-4">
    <label class="control-label" style="width:100%">&nbsp;</label>
    <button class="btn btn-primary" type="submit" style="float:left"> Guardar grupo</button>
  </div>
</form>

<script type="text/javascript" charset="utf8" src="<?=base_url('assets/js/editar.js')?>"></script>
<?php if ($metod == 'patch') { ?>
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/datatable/js/jquery.dataTables.js')?>"></script>
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/datatable/js/dataTables.buttons.min.js')?>"></script>
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/datatable/js/jszip.min.js')?>"></script>
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/datatable/js/buttons.html5.min.js')?>"></script>
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/datatable/js/dataTables.bootstrap.min.js')?>"></script>
 
<h2>Asignar docentes</h2>
<hr>
<table class="table" id="datatable">
  <thead><tr><th></th><th></th><th></th><th></th><th></th></tr></thead>
  <tbody></tbody>
</table>

<script type="text/javascript" charset="utf8">
  $(document).ready(function() {
    
    var columns = []
    
    columns.push({ "title":"ID", "data":"usuario_id", "visible":false, })
    columns.push({ "title":"Rut", "data":"usuario_rut" })
    columns.push({ "title":"Nombres", "data":"usuario_nombre" })
    columns.push({ "title":"Apellido Paterno", "data":"usuario_appat" })
    columns.push({ "targets":-1, "orderable": false, "data": null, "defaultContent":"<button class='btn btn-primary' data-placement='top' data-title='tooltip' title='Asignar docente'><i class='fa fa-plus'></i></button><button style='display:none' class='btn btn-danger' data-title='tooltip' data-placement='top' title='Quitar docente'><i class='fa fa-times'></i></button>" })
    
    $('#datatable')
      .DataTable({
      "ajax":
      {
        "url":"/plataforma/index.php/usuarios/json",
        "type":"GET",
        "dataSrc":""
      },
      "columns":columns,
      "initComplete":function()
      {
        $.ajax({
          "type":"get",
          "url":"/plataforma/index.php/grupos/docentes/json?grupo_id=<?=$this->uri->segment(3)?>",
          "dataType":"json"
        })
        .done(function(assigns) {
          $('#datatable').DataTable().rows( function ( idx, data, node ) {
            $.each(assigns, function(i, e) {
              if (e['usuario_id'] == data['usuario_id'])
              {
                $(node).addClass('active').children('td').children('.btn-primary').hide()
                $(node).addClass('active').children('td').children('.btn-danger').show()
              }
            })

          })
        })
        
        $("#datatable tbody")
          .on(
          'click',
          'button.btn-primary',
          function()
          {
            var button = $(this).attr('disabled', true)
            $.ajax({
              "type":"post",
              "url":"/plataforma/index.php/grupos/docentes",
              "data": 
              { 
                "grupo_id": "<?=$this->uri->segment(3)?>",
                "usuario_id": $("#datatable").DataTable().row( $(this).parents('tr') ).data()['usuario_id']
              } 
            })
            .done(function() {
              $(button).toggle().next().show()
              $(button).attr('disabled', false)
              $(button).parents('tr').addClass('active')
            })
            .fail(function() {
              $(button).attr('disabled', false)
            })
          }
        )
        .on(
        'click',
        'button.btn-danger',
        function()
        {
          var button = $(this).attr('disabled', true)
          $.ajax({
            "type":"delete",
            "url":"/plataforma/index.php/grupos/docentes",
            "data": 
            { 
              "grupo_id": "<?=$this->uri->segment(3)?>",
              "usuario_id": $("#datatable").DataTable().row( $(this).parents('tr') ).data()['usuario_id']
            } 
          })
          .done(function() {
            $(button).toggle().prev().show()
            $(button).attr('disabled', false)
            $(button).parents('tr').removeClass('active')
          })
          .fail(function() {
            $(button).attr('disabled', false)
          })
        }
      )
      },
      "language":
      {
        "lengthMenu": "<strong>Mostrar </strong> _MENU_",
        "info":"Mostrando _START_ hasta _END_ de <strong>_TOTAL_ docente(s)</strong>",
        "search":"<strong>Filtrar </strong> _INPUT_",
        "infoFiltered":"(filtrado de _MAX_ )",
        "zeroRecords": "Sin alumnos.",
        "paginate":
        {
          "previous":"Anterior",
          "next":"Siguiente"
        }
      }
    })
    
  })
</script>
<?php } ?>