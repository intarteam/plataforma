<!-- Validator -->
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/js/validator.js')?>"></script>
<!-- Notify -->
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/js/jquery.growl.js')?>"></script>

<script type="text/javascript" charset="utf8" src="<?=base_url('assets/datatable/js/jquery.dataTables.js')?>"></script>
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/datatable/js/dataTables.buttons.min.js')?>"></script>
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/datatable/js/jszip.min.js')?>"></script>
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/datatable/js/buttons.html5.min.js')?>"></script>
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/datatable/js/dataTables.bootstrap.min.js')?>"></script>

<style>
  .form-control-feedback {
    right: 15;
  }
  select + .form-control-feedback {
    right: 25;
  }
  form button {
    float: right;
    margin-right: 15;
  }
  .tab-pane {
    padding: 20px 10px;
  }
  form.row {
    margin-right: -15px;
    margin-left: -15px;
    margin-top: 0px;
  }
  .bs-example {
    padding-top: 20px;
  }
  #datatable tbody td:first-child {
    border-left: 1px solid #c5d2e7 ;
  }
  #datatable tbody td:last-child {
    border-right: 1px solid #c5d2e7 ;
    text-align: right;
  }
  #datatable tbody tr:last-child td {
    border-bottom: 1px solid #c5d2e7 ;
  }
  #datatable tbody td {
    border-color: #ddd ;
    padding: 5px 8px;
  }
   thead th {
    background-color: #eff3f8;
    border-color: #c5d2e7 ;
    color: #4a72b2;
  }
  #datatable .btn {
    padding: 4px 12px;
  }
  #datatable .btn-edit {
    color: #0085ff;
  }
  #datatable .btn-chart {
    color: #4a72b2;
  }
  #datatable {
    padding: 15px 0px;
  }
  .table-hover>tbody>tr:hover {
    background-color: #eff3f8;
    color: #4a72b2;
  }
  .dataTables_length label {
    padding-right: 8px;
  }
  .dataTables_length select {
    width: auto !important;
  }
  #datatable tbody tr.active>td {
    background-color: #eff3f8 !important;
    color: #4a72b2;
  }
  .not-active {
   pointer-events: none;
   cursor: default;
  }
</style>

<div class="bs-example bs-example-tabs" data-example-id="togglable-tabs">
  <ul class="nav nav-tabs" id="myTabs" role="tablist">
    
    <li role="presentation" class="active">
      <a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true"><h4><?=$title?></h4></a>
    </li>
    <?php if ($metod != 'post') { ?>
    <li role="presentation">
      <a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile"><h4>Condición</h4></a>
    </li>
    </li><?php } ?>
    <?php if ($metod != 'patch') { ?> 
    <li role="presentation">
      <a href="#import" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile"><h4>Importar alumnos</h4></a>
    </li><?php } ?>
    
  </ul>
  <div class="tab-content" id="myTabContent">
    
    <div class="tab-pane fade in active" role="tabpanel" id="home" aria-labelledby="home-tab">
      <form class="row" autocomplete="off" method="<?=$metod?>" source="<?=$resrc?>">
        <div class="form-group has-feedback col-md-4">
          <label class="control-label">Nombres</label>
          <input name="alumno_nombres" type="text" placeholder="Nombre(s)" class="form-control" required>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        
        <div class="form-group has-feedback col-md-4">
          <label class="control-label">Apellido Paterno</label>
          <input name="alumno_apellidop" type="text" placeholder="Apellido Paterno" class="form-control" required>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        
        <div class="form-group has-feedback col-md-4">
          <label class="control-label">Apellido Materno</label>
          <input name="alumno_apellidom" type="text" placeholder="Apellido Materno" class="form-control" required>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        
        <div class="form-group has-feedback col-md-4">
          <label class="control-label">Rut</label>
          <input name="alumno_rut" type="text" placeholder="12345678-9" class="form-control" required<?php if($this->session->userdata('tipo_id') > 1 and $metod == 'patch') { echo ' disabled'; } ?>>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>

      <div class="form-group has-feedback col-md-2">
          <label class="control-label">Fecha de Nacimiento</label>
          <input name="alumno_nacimiento" type="text" placeholder="2000-01-01" class="form-control" required>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
      </div>
        
        <div class="form-group has-feedback col-md-2">
          <label class="control-label">Codigo</label>
          <input name="alumno_codigo" type="text" placeholder="ABCDE-001" class="form-control" required<?php if($this->session->userdata('tipo_id') > 1) { echo ' disabled'; } ?>>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        
        <div class="form-group has-feedback col-md-4">
          <label class="control-label">Responsable</label>
          <input name="alumno_responsable" type="text" placeholder="Nombre(s) y Apellido(s)" class="form-control" required>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        
        <div class="form-group has-feedback col-md-4">
          <label class="control-label">Teléfono</label>
          <input name="alumno_fonocontacto" type="text" placeholder="+56 9 87654321" class="form-control" required>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        
        <div class="form-group has-feedback col-md-4">
          <label class="control-label">Grupo</label>
          <select name="grupo_id" type="text" class="form-control" required>
            <option>Ninguno</option>
            <?php
              foreach ($this->db->query('select * from grupo where institucion_id = '.$this->session->userdata('institucion_id'))->result() as $grupo)
              {
                echo '<option value="'.$grupo->grupo_id.'">'.$grupo->grupo_identificador.'</option>';
              }
            ?>
          </select>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        
        <div class="form-group has-feedback col-md-12">
          <h4>Ubicación</h4>
        </div>
        
        <div class="form-group has-feedback col-md-3">
          <label class="control-label">Institución</label>
          <select name="institucion_id" type="text" class="form-control" required<?php if($this->session->userdata('tipo_id') > 1) { echo ' disabled'; } ?>>
            <?php
              foreach ($this->db->query('select * from institucion')->result() as $institucion)
              {
                  if ($this->session->userdata('tipo_id') > 1 and $this->session->userdata('institucion_id') == $institucion->institucion_id) {
                      echo '<option value="'.$institucion->institucion_id.'" selected>'.$institucion->institucion_nombre.'</option>';
                  } else {
                      echo '<option value="'.$institucion->institucion_id.'">'.$institucion->institucion_nombre.'</option>';
                  }
              }
            ?>
          </select>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        
        <div class="form-group has-feedback col-md-3">
          <label class="control-label">Comuna</label>
          <select name="comuna_id" type="text" class="form-control" required>
            <?php
              foreach ($this->db->query('select * from comuna')->result() as $comuna)
              {
                echo '<option value="'.$comuna->comuna_id.'">'.$comuna->comuna_nombre.'</option>';
              }
            ?>
          </select>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        
        <div class="form-group has-feedback col-md-6">
          <label class="control-label">Dirección</label>
          <input name="alumno_direccion" type="text" placeholder="Nombre de calle, #Numero de casa" class="form-control" required>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        
        <button class="btn btn-primary" type="submit"> <?php if($metod == 'post') { echo 'Agregar'; } else { echo 'Guardar'; } ?> alumno </button>
      </form>
    </div>
    <?php if ($metod != 'post') { ?>
    <div class="tab-pane fade" role="tabpanel" id="profile" aria-labelledby="profile-tab">
      <table class="table" id="datatable">
        <thead><tr><th></th><th></th><th></th><th></th></tr></thead>
        <tbody></tbody>
      </table>
    </div>
    <?php } ?>
    <?php if ($metod != 'patch') { ?>
    <div class="tab-pane fade" role="tabpanel" id="import" aria-labelledby="profile-tab">
      <div class="row">
        <div class="col-md-12">
          <div class="well well-sm">
            Esta sección esta creada para agregar una lista de alumnos a partir de un archivo en formato Excel
          </div>
        </div>
        
        <div class="form-group has-feedback col-md-3">
          <label class="control-label">Institución</label>
          <select name="institucion_id" type="text" placeholder="00" class="form-control" required<?php if($this->session->userdata('tipo_id') > 1) { echo ' disabled'; } ?>>
            <?php
              foreach ($this->db->query('select * from institucion')->result() as $institucion)
              {
                echo '<option value="'.$institucion->institucion_id.'">'.$institucion->institucion_nombre.'</option>';
              }
            ?>
          </select>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        
        <div class="col-md-5">
          <label class="control-label">Seleccionar archivo</label>
          <input class="form-control" type="file" id="my_file_input">
        </div>
        
        <div class="col-md-4" id="importar" style="display:none">
          <label class="control-label">Revise los alumnos antes de agregarlos</label><br>
          <a class="btn btn-primary" type="button">Agregar alumnos</a>
        </div>
        
        <div class="col-md-12">
          <br>
          <table class="table table-condensed" id="importable" style="display:none">
            <thead>
              <tr>
                <th>Rut</th>
                <th>Nombres</th>
                <th>A. Paterno</th>
                <th>A. Materno</th>
                <th>Nacimiento</th>
                <th>Responsable</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>
      </div>
    </div>
    <?php } ?>
  </div>
</div>

<script type="text/javascript" charset="utf8" src="<?=base_url('assets/js/editar.js')?>"></script>
<?php if ($metod != 'post') { ?>
<script type="text/javascript" charset="utf8">
  $(document).ready(function() {
    
    var columns = []
    
    columns.push({ "title":"ID", "data":"diagnostico_id", "visible":false, })
    columns.push({ "title":"Diagnostico", "data":"diagnostico_nombre" })
    columns.push({ "title":"Tipo", "data":"tipod_nombre" })
    columns.push({ "targets":-1, "orderable": false, "data": null, "defaultContent":"<button class='btn btn-primary'><i class='fa fa-plus'></i></button><button style='display:none' class='btn btn-danger'><i class='fa fa-times'></i></button>" })
    
    $('#datatable')
      .DataTable({
      "ajax":
      {
        "url":"/plataforma/index.php/alumnos/diagnosticos/json",
        "type":"GET",
        "dataSrc":""
      },
      "columns":columns,
      "columnDefs":
      [
        {
          "width":"10%",
          "targets":1
        }
      ],
      "initComplete":function()
      {
        render([])
      },
      "language":
      {
        "lengthMenu": "<strong>Mostrar </strong> _MENU_",
        "info":"Mostrando _START_ hasta _END_ de _TOTAL_ ",
        "search":"<strong>Filtrar </strong> _INPUT_",
        "infoFiltered":"(filtrado de _MAX_ )",
        "zeroRecords": "Sin resultados ",
        "paginate":
        {
          "previous":"Anterior",
          "next":"Siguiente"
        }
      }
    })
  })

  function render(filters)
  {
    $("#datatable tbody")
      .on(
      'click',
      'button.btn-primary',
      function()
      {
        var button = $(this).attr('disabled', true)
        $.ajax({
          "type":"post",
          "url":"/plataforma/index.php/alumnos/diagnosticos?" + $('form').attr('source').split('?')[1],
          "data": 
          { 
            "diagnostico_id": $("#datatable").DataTable().row( $(this).parents('tr') ).data()['diagnostico_id'],
            "alumno_id": $('form').attr('source').split('=')[1]
          } 
        })
        .done(function() {
          $(button).toggle().next().show()
          $(button).attr('disabled', false)
          $(button).parents('tr').addClass('active')
        })
        .fail(function() {
          $(button).attr('disabled', false)
        })
      }
    )
      .on(
      'click',
      'button.btn-danger',
      function()
      {
        var button = $(this).attr('disabled', true)
        $.ajax({
          "type":"delete",
          "url":"/plataforma/index.php/alumnos/diagnosticos?" + $('form').attr('source').split('?')[1],
          "data": 
          { 
            "diagnostico_id": $("#datatable").DataTable().row( $(this).parents('tr') ).data()['diagnostico_id'],
            "alumno_id": $('form').attr('source').split('=')[1]
          } 
        })
        .done(function() {
          $(button).toggle().prev().show()
          $(button).attr('disabled', false)
          $(button).parents('tr').removeClass('active')
        })
        .fail(function() {
          $(button).attr('disabled', false)
        })
      }
    )

    $('.dataTables_length, .dataTables_filter, .dataTables_info, .dataTables_paginate').css('padding', '0')
    $('.dataTables_info, .dataTables_paginate').addClass('col-sm-6')
    $('.dataTables_length').addClass('col-sm-9')
    $('.dataTables_filter').addClass('col-sm-3').css('float', 'right').css('width', 'auto')
    $('.dataTables_paginate').css('float', 'right').css('width', 'auto')

    $('[data-toggle="tooltip"]').tooltip()
    $('#datatable').css('width', '100%')
    
    $.ajax({
      "type":"get",
      "url":"/plataforma/index.php/alumnos/diagnosticos/json?" + $('form').attr('source').split('?')[1],
      "dataType":"json"
    })
    .done(function(assign) {

      $('#datatable').DataTable().rows( function ( idx, data, node ) {

        $.each(assign, function(i, e) {
          
          if (e['diagnostico_id'] == data['diagnostico_id'])
          {
            $(node).addClass('active').children('td').children('.btn-primary').hide()
            $(node).addClass('active').children('td').children('.btn-danger').show()
          }
        })

      })

    })
    
    $('.progress').fadeOut()
  }
</script>
<?php } ?>
<?php if ($metod != 'patch') { ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.0/xlsx.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.0/jszip.js"></script>

<script type="text/javascript" charset="utf8">
  
  $(document).ready(function() {
    
    var oFileIn; var oJS;

    $(function() {
        oFileIn = document.getElementById('my_file_input');
        if(oFileIn.addEventListener) {
            oFileIn.addEventListener('change', filePicked, false);
        }
    });


    function filePicked(oEvent) {
        // Get The File From The Input
        var oFile = oEvent.target.files[0];
        var sFilename = oFile.name;
        // Create A File Reader HTML5
        var reader = new FileReader();
      $('#importable').fadeOut()

        // Ready The Event For When A File Gets Selected
        reader.onload = function(e) {
            var data = e.target.result;
            var cfb = XLS.CFB.read(data, {type: 'binary'});
            var wb = XLS.parse_xlscfb(cfb);
            // Loop Over Each Sheet
            wb.SheetNames.forEach(function(sheetName) {
                // Obtain The Current Row As CSV
                var sCSV = XLS.utils.make_csv(wb.Sheets[sheetName]);   
                oJS = XLS.utils.sheet_to_row_object_array(wb.Sheets[sheetName]);   
                $('#importable tbody').empty()
                oJS.forEach(function(row) {
                  
                  $('#importable').append('<tr><td>' + row.alumno_rut + '</td><td>' + row.alumno_nombres + '</td><td>' + row.alumno_apellidop + '</td><td>' + row.alumno_apellidom + '</td><td>' + row.alumno_nacimiento + '</td><td>' + row.alumno_responsable + '</td></tr>')
                
                })
                
                $('#importable, #importar').fadeIn()
            });
        };

        // Tell JS To Start Reading The File.. You could delay this if desired
        reader.readAsBinaryString(oFile);
    }
    
    $('#importar').on('click', 'a', function(e){
      
      $('.progress').fadeIn()
      
      console.log(JSON.stringify(oJS))
      
      $.ajax({ 
        type: 'post',
        url: '/plataforma/index.php/alumnos/importar?institucion_id=' + $('#import select[name="institucion_id"]').val(), 
        data: JSON.stringify(oJS),
        dataType: 'json'
      })
      .always(function() {
        $('.progress').fadeOut()  
      })
      .done(function(data) {
        $.growl.notice(data)
      })
      .fail(function(data) {
        var message = ''
        $.each(JSON.parse(data.responseText), function(i, e) {
          message = message + e.message + '<br>'
        })
        
        $.growl.error({"title":"Ouch!", "message":message})
      })
      
    })
    
  })

</script>
<?php } ?>
