<!-- Validator -->
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/js/validator.js')?>"></script>
<!-- Notify -->
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/js/jquery.growl.js')?>"></script>

<style>
  .row div.col-lg-12 h1 {
    padding-top: 25px;
  }
  .form-control-feedback {
    right: 15;
  }
  form button {
    float: right;
    margin-right: 15;
  }
</style>

<h1><?=$title?></h1>
<hr>
<form class="row" autocomplete="off" method="<?=$metod?>" source="<?=$resrc?>">
  <div class="form-group has-feedback col-md-2">
    <label class="control-label">ID</label>
    <input name="aplicacion_id" type="text" placeholder="ID" class="form-control" required>
    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
  </div>
  
  <div class="form-group has-feedback col-md-10">
    <label class="control-label">Nombre</label>
    <input name="aplicacion_nombre" type="text" placeholder="Nombre de la aplicacion" class="form-control" required>
    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
  </div>
  
  <button class="btn btn-primary" type="submit"> Guardar aplicacion</button>
</form>

<script type="text/javascript" charset="utf8" src="<?=base_url('assets/js/editar.js')?>"></script>
