<!-- Validator -->
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/js/validator.js')?>"></script>
<!-- Notify -->
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/js/jquery.growl.js')?>"></script>

<style>
  .form-control-feedback {
    right: 15;
  }
  select + .form-control-feedback {
    right: 25;
  }
  form button {
    float: right;
    margin-right: 15;
  }
  dl {
    margin-top: 0;
    margin-bottom: 0px;
    font-size: 14px;
  }
</style>

<h1><?=$title?></h1>
<hr>
<form class="row" autocomplete="off" method="<?=$metod?>" source="<?=$resrc?>"> 
  
  <div class="form-group has-feedback col-md-2"</div>
    <label class="control-label">Rut</label>
    <input name="institucion_rut" type="text" placeholder="12345678-9" class="form-control" required<?php if($this->session->userdata('tipo_id') > 1) { echo ' disabled'; } ?>>
    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
  </div>

  <div class="form-group has-feedback col-md-4">
    <label class="control-label">Nombre</label>
    <input name="institucion_nombre" type="text" placeholder="Nombre" class="form-control" required<?php if($this->session->userdata('tipo_id') > 1) { echo ' disabled'; } ?>>
    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
  </div>
  
  <div class="form-group has-feedback col-md-6">
    <label class="control-label">Razón Social</label>
    <input name="institucion_razonsocial" type="text" placeholder="Razon Social" class="form-control" required<?php if($this->session->userdata('tipo_id') > 1) { echo ' disabled'; } ?>>
    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
  </div>

  <div class="form-group has-feedback col-md-3">
    <label class="control-label">Tipo</label>
    <select name="tiposub_id" type="text" placeholder="00" class="form-control" required<?php if($this->session->userdata('tipo_id') > 1) { echo ' disabled'; } ?>>
      <?php
        foreach ($this->db->query('select * from tipo_subvencion')->result() as $tipo)
        {
          echo '<option value="'.$tipo->tiposub_id.'">'.$tipo->tiposub_nombre.'</option>';
        }
      ?>
    </select>
    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
  </div>
  
  <div class="form-group has-feedback col-md-9">
    <label class="control-label">Logo o imagen</label>
    <input name="institucion_imagen" type="file" placeholder="Apellido Paterno" class="form-control"<?php if($this->session->userdata('tipo_id') > 2) { echo ' disabled'; } ?>>
    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
  </div>
  
  <div class="form-group has-feedback col-md-12">
    <h4>Ubicación</h4>
  </div>
  
  <div class="form-group has-feedback col-md-3">
    <label class="control-label">Comuna</label>
    <select name="comuna_id" type="text" placeholder="00" class="form-control" required<?php if($this->session->userdata('tipo_id') > 2) { echo ' disabled'; } ?>>
      <?php
        foreach ($this->db->query('select * from comuna')->result() as $comuna)
        {
          echo '<option value="'.$comuna->comuna_id.'">'.$comuna->comuna_nombre.'</option>';
        }
      ?>
    </select>
    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
  </div>
  
  <div class="form-group has-feedback col-md-9">
    <label class="control-label">Dirección</label>
    <input name="institucion_direccion" type="text" placeholder="Dirección" class="form-control" required<?php if($this->session->userdata('tipo_id') > 2) { echo ' disabled'; } ?>>
    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
  </div>

  <?php if($this->session->userdata('tipo_id') < 3) { ?><button class="btn btn-primary" type="submit">Guardar cambios</button><?php } ?>
  <?php if(is_numeric($this->uri->segment(3))) { ?><button class="btn btn-default" type="button" data-toggle="modal" data-target="#myModal">Ver Contrato</button><?php } ?>
</form>

<script type="text/javascript" charset="utf8" src="<?=base_url('assets/js/editar.js')?>"></script>

<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Detalles del contrato</h4>
      </div>
      <div class="modal-body">
        <?php
  
          if (is_numeric($this->uri->segment(3))) {
            $contrato = $this->db->query('select * from contrato inner join plan on plan.plan_id = contrato.plan_id where institucion_id = '.$this->uri->segment(3))->result();

            if ($contrato)
            {
              $contrato = $contrato[0];

              echo '<dl class="dl-horizontal">
                      <dt>Representante</dt>
                      <dd>'.$contrato->contrato_representante.'</dd>
                      <dt>Fecha de inicio</dt>
                      <dd>'.date_format(date_create($contrato->contrato_inicio), 'd/m/Y').'</dd>
                      <dt>Fecha de término</dt>
                      <dd>'.date_format(date_create($contrato->contrato_fin), 'd/m/Y').'</dd>
                      <br>
                      <dt>Plan</dt>
                      <dd>'.$contrato->plan_nombre.'</dd>
                      <dt>Precio</dt>
                      <dd>$ '.$contrato->plan_precio.'</dd>
                      <dt>Duración</dt>
                      <dd>'.$contrato->plan_duracion.' dias</dd>
                    </dl>';
            }
          }     
        
        ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->