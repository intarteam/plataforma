<!-- Validator -->
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/js/validator.js')?>"></script>
<!-- Notify -->
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/js/jquery.growl.js')?>"></script>

<script type="text/javascript" charset="utf8" src="<?=base_url('assets/datatable/js/jquery.dataTables.js')?>"></script>
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/datatable/js/dataTables.buttons.min.js')?>"></script>
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/datatable/js/jszip.min.js')?>"></script>
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/datatable/js/buttons.html5.min.js')?>"></script>
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/datatable/js/dataTables.bootstrap.min.js')?>"></script>

<link href="<?=base_url('assets/datatable/css/dataTables.bootstrap.css')?>" rel="stylesheet">

<style>
  .form-control-feedback {
    right: 15;
  }
  select + .form-control-feedback {
    right: 25;
  }
  form button {
    float: right;
    margin-right: 15;
  }
  .tab-pane {
    padding: 20px 10px;
  }
  form.row {
    margin-right: -15px;
    margin-left: -15px;
    margin-top: 0px;
  }
  .bs-example {
    padding-top: 40px;
  }
  tbody td:first-child {
    border-left: 1px solid #fff ;
  }
  tbody td:last-child {
    border-right: 1px solid #fff ;
    text-align: right;
  }
  tbody tr:last-child td {
    border-bottom: 1px solid #fff ;
  }
  #datatable tbody td, #datatable_alumnos tbody td {
    border-color: #e7edf5 ;
    padding: 5px 8px;
  }
  #datatable thead th, #datatable_alumnos thead th {
    background-color: #e7edf5;
    border-color: #4a72b2 ;
    color: #4a72b2;
  }
  table .btn {
    padding: 4px 12px;
  }
  table .btn-edit, #datatable .btn-chart, #datatable .btn-report {
    color: #4a72b2;
  }
  table {
    padding: 15px 0px;
  }
  .table-hover>tbody>tr:hover {
    background-color: #eff3f8;
    color: #4a72b2;
  }
  .dataTables_length label {
    padding-right: 8px;
  }
  .dataTables_length select {
    width: auto !important;
  }
  #datatable tbody tr.active>td {
    background-color: #eff3f8 !important;
    color: #4a72b2;
  }
  .not-active {
   pointer-events: none;
   cursor: default;
  }
  #togglable-tabs {
    margin-top: 15px;
  }
  td button.btn {
    margin-left: -1px;
  }
  .btn-alumno {
    top: 15px;
    right: 25px;
    position: absolute;
  }
</style>

<div id="togglable-tabs">
  <ul class="nav nav-tabs" id="myTabs" role="tablist">
    <li role="presentation" class="active">
      <a href="#curso" role="tab" id="home-tab" data-toggle="tab" aria-controls="curso" aria-expanded="true"><h4><?=$title?></h4></a>
    </li>
    <?php if ($metod == 'patch') { ?>
    <li role="presentation">
      <a href="#curso_alumnos" role="tab" id="profile-tab" data-toggle="tab" aria-controls="curso_alumnos"><h4>Alumnos</h4></a>
    </li>
    <?php } ?>
  </ul>
  <div class="tab-content" id="myTabContent">
    <div class="tab-pane fade in active" role="tabpanel" id="curso" aria-labelledby="home-tab">
      <form class="row" autocomplete="off" method="<?=$metod?>" source="<?=$resrc?>">
        <div class="form-group has-feedback col-md-6">
          <label class="control-label">Institución</label>
          <select name="institucion_id" type="text" class="form-control" required<?php if($this->session->userdata('tipo_id') > 1) { echo ' disabled'; } ?>>
            <?php
              foreach ($this->db->query('select * from institucion ')->result() as $institucion)
              {
                  if ($metod == 'post' and $institucion->institucion_id == $this->session->userdata('institucion_id')) {
                      echo '<option value="'.$institucion->institucion_id.'" selected>'.$institucion->institucion_nombre.'</option>';
                  } else {
                      echo '<option value="'.$institucion->institucion_id.'">'.$institucion->institucion_nombre.'</option>';
                  }
              }
            ?>
          </select>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>

        <div class="form-group has-feedback col-md-6">
          <label class="control-label">Docente</label>
          <select name="usuario_id" type="text" class="form-control" required<?php if($this->session->userdata('tipo_id') > 2) { echo ' disabled'; } ?>>
            <?php
                $aux_where = '';
                if ($this->session->userdata('tipo_id') > 1) {
                    $aux_where = ' and institucion_id = '.$this->session->userdata('institucion_id');
                }

                foreach ($this->db->query('select * from usuario where tipo_id > 1'.$aux_where)->result() as $docente)
                {
                    if ($metod == 'post' and $docente->usuario_id == $this->session->userdata('usuario_id')) {
                        echo '<option value="'.$docente->usuario_id.'" selected>'.$docente->usuario_nombre.' '.$docente->usuario_appat.'</option>';
                    } else {
                        echo '<option value="'.$docente->usuario_id.'">'.$docente->usuario_nombre.' '.$docente->usuario_appat.'</option>';
                    }
                }
            ?>
          </select>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>

        <div class="form-group has-feedback col-md-2">
          <label class="control-label">Grado</label>
          <input name="curso_grado" type="text" placeholder="1" class="form-control">
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>

        <div class="form-group has-feedback col-md-2">
          <label class="control-label">Letra</label>
          <input name="curso_letra" type="text" placeholder="A" class="form-control" required>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>

        <div class="form-group has-feedback col-md-3">
          <label class="control-label">Periodo</label>
          <input name="curso_periodo" type="text" placeholder="<?=date('Y-m-d')?>" class="form-control" required>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>

        <div class="form-group has-feedback col-md-12">
          <button class="btn btn-primary" type="submit"> Guardar datos generales del curso</button>
        </div>
      </form>
    </div>
    <div class="tab-pane fade" role="tabpanel" id="curso_alumnos" aria-labelledby="profile-tab">
      <table class="table" id="datatable">
        <thead><tr><th></th><th></th><th></th><th></th><th></th></tr></thead>
        <tbody></tbody>
      </table>
    </div> 
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_observacion">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Observación del alumno</h4>
      </div>
      <div class="modal-body row">
        <div class="form-group col-md-12">
          <textarea id="cur_al_observacion" class="form-control" rows="5">Observaciones del curso en relación al alumno...</textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-success">Guardar observación</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="modal_retiro">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Retirar alumno</h4>
      </div>
      <div class="modal-body row">
        <div class="form-group col-md-12">
          <textarea id="retiro_motivo" class="form-control" rows="5">Motivo del retiro del alumno...</textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-warning">Retirar alumno</button>
        <?php if($this->session->userdata('tipo_id') < 2) { ?><button type="button" style="float:left" class="btn btn-danger">Quitar alumno</button><?php } ?>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="modal_alumnos">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Agregar alumnos</h4>
      </div>
      <div class="modal-body">
        <table class="table" id="datatable_alumnos">
          <thead><tr><th></th><th></th><th></th><th></th><th></th><th></th></tr></thead>
          <tbody></tbody>
        </table>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript" charset="utf8" src="<?=base_url('assets/js/editar.js')?>"></script>
<script type="text/javascript" charset="utf8">
  $(document).ready(function() {
    
    var columns = []
    
    columns.push({ "title":"ID", "data":"alumno_id", "visible":false, })
    columns.push({ "title":"Rut", "data":"alumno_rut" })
    columns.push({ "title":"Nombres", "data":"alumno_nombres" })
    columns.push({ "title":"Apellido Paterno", "data":"alumno_apellidop" })
    columns.push({ "targets":-1, "orderable": false, "data": null, "defaultContent":"<button class='btn btn-success' data-toggle='modal' data-placement='top' data-title='tooltip' title='Observacion del alumno' data-target='#modal_observacion'><i class='fa fa-comment'></i></button><button class='btn btn-warning' data-title='tooltip' data-placement='top' title='Retirar alumno' data-toggle='modal' data-target='#modal_retiro'><i class='fa fa-sign-out'></i></button>" })
    
    $('#datatable')
      .DataTable({
      "ajax":
      {
        "url":"/plataforma/index.php/cursos/alumnos/json?" + $('form').attr('source').split('?')[1],
        "type":"GET",
        "dataSrc":""
      },
      "columns":columns,
      "initComplete":function()
      {
        render([])
      },
      "language":
      {
        "lengthMenu": "<strong>Mostrar </strong> _MENU_",
        "info":"Mostrando _START_ hasta _END_ de <strong>_TOTAL_ alumno(s)</strong>",
        "search":"<strong>Filtrar </strong> _INPUT_",
        "infoFiltered":"(filtrado de _MAX_ )",
        "zeroRecords": "Sin alumnos.",
        "paginate":
        {
          "previous":"Anterior",
          "next":"Siguiente"
        }
      }
    })
    
    columns = []
    
    columns.push({ "title":"ID", "data":"alumno_id", "visible":false, })
    columns.push({ "title":"Rut", "data":"alumno_rut" })
    columns.push({ "title":"Nombres", "data":"alumno_nombres" })
    columns.push({ "title":"Apellido Paterno", "data":"alumno_apellidop" })
    columns.push({ "title":"Curso", "data":"curso_grado_letra" })
    columns.push({ "targets":-1, "orderable": false, "data": null, "defaultContent":"<button class='btn btn-primary' data-placement='top' data-title='tooltip' title='Agregar alumno'><i class='fa fa-plus'></i></button>" })
    
    $('#datatable_alumnos')
      .DataTable({
      "ajax":
      {
        "url":"/plataforma/index.php/alumnos/json?curso_id=null",
        "type":"GET",
        "dataSrc":""
      },
      "columns":columns,
      "initComplete":function()
      {
        $('#datatable_alumnos')
          .on(
          'click',
          'button.btn-primary',
          function()
          {
            $('.progress').fadeIn()
            
            $.ajax({ 
              type: 'post',
              url: '/plataforma/index.php/cursos/alumnos', 
              data: { "alumno_id":$("#datatable_alumnos").DataTable().row( $(this).parents('tr') ).data()['alumno_id'], "curso_id":"<?=$this->uri->segment(3)?>" }
            })
            .always(function() {
              $('.progress').fadeOut()  
            })
            .done(function() {
              $.growl.notice({ "title":"Yeah!", "message":"Alumno agregado." })
              $('#datatable').DataTable().ajax.reload()
              $('#datatable_alumnos').DataTable().ajax.reload()
            })
            .fail(function() {
              $.growl.error({ "title":"Ouch!", "message":"No se puede agregar el alumno." })
            })
          }
        )
        
        $('#datatable_alumnos').css('width', '100%')
        $('[data-title="tooltip"]').tooltip()
      },
      "language":
      {
        "lengthMenu": "<strong>Mostrar </strong> _MENU_",
        "info":"Mostrando _START_ hasta _END_ de <strong>_TOTAL_ alumno(s)</strong>",
        "search":"<strong>Filtrar </strong> _INPUT_",
        "infoFiltered":"(filtrado de _MAX_ )",
        "zeroRecords": "Sin alumnos.",
        "paginate":
        {
          "previous":"Anterior",
          "next":"Siguiente"
        }
      }
    })

    $('#modal_observacion')
      .on(
      'click',
      'button.btn-success',
      function()
      {
        $('.progress').fadeIn()

        $.ajax({ 
          type: 'patch',
          url: '/plataforma/index.php/cursos/alumnos?alumno_id=' + $('#modal_observacion textarea').attr('alumno_id') + '&curso_id=<?=$this->uri->segment(3)?>', 
          data: { "cur_al_observacion":$('#cur_al_observacion').val() }
        })
        .always(function() {
          $('.progress').fadeOut()  
        })
        .done(function() {
          $.growl.notice({ "title":"Yeah!", "message":"Observación del alumno actualizada." })
        })
        .fail(function() {
          $.growl.error({ "title":"Ouch!", "message":"No se puede actualizar la observación del alumno." })
        })
      }
    )
    
    $('#modal_retiro')
      .on(
      'click',
      'button.btn-warning',
      function()
      {
        $('.progress').fadeIn()

        $.ajax({ 
          type: 'patch',
          url: '/plataforma/index.php/cursos/alumnos?alumno_id=' + $('#modal_retiro textarea').attr('alumno_id') + '&curso_id=<?=$this->uri->segment(3)?>', 
          data: { "retiro_motivo":$('#retiro_motivo').val() }
        })
        .always(function() {
          $('.progress').fadeOut()  
        })
        .done(function() {
          $.growl.warning({ "title":"Yeah!", "message":"Alumno retirado, motivo el retiro actualizado." })
        })
        .fail(function() {
          $.growl.error({ "title":"Ouch!", "message":"No se puede retirar al alumno." })
        })
      }
    )
    
    $('#modal_retiro')
      .on(
      'click',
      'button.btn-danger',
      function()
      {
        $('.progress').fadeIn()

        $.ajax({ 
          type: 'delete',
          url: '/plataforma/index.php/cursos/alumnos?alumno_id=' + $('#modal_retiro textarea').attr('alumno_id') + '&curso_id=<?=$this->uri->segment(3)?>'
        })
        .always(function() {
          $('.progress').fadeOut()  
        })
        .done(function() {
          $('#modal_retiro').modal('hide')
          $.growl.error({ "title":"Yeah!", "message":"Alumno eliminado del curso." })
          $('#datatable').DataTable().ajax.reload()
          $('#datatable_alumnos').DataTable().ajax.reload()
        })
        .fail(function() {
          $.growl.error({ "title":"Ouch!", "message":"No se puede eliminar al alumno." })
        })
      }
    )
    

  })

  function render(filters)
  {
    $("#datatable tbody")
      .on(
      'click',
      'button.btn-success',
      function()
      {
        $.ajax({
          "type":"get",
          "url":"/plataforma/index.php/cursos/alumnos/json?alumno_id=" + $("#datatable").DataTable().row( $(this).parents('tr') ).data()['alumno_id'],
          "dataType":"json"
        })
        .done(function(data) {
          if (data.length > 0) {
            $('#modal_observacion .modal-title').html('Observación del alumno <strong>' + data[0].alumno_nombres + ' ' + data[0].alumno_apellidop + '</strong>')
            $('#cur_al_observacion').val(data[0].cur_al_observacion).attr('alumno_id', data[0].alumno_id)
          }
        })
      }
    )
      .on(
      'click',
      'button.btn-warning',
      function()
      {
        $.ajax({
          "type":"get",
          "url":"/plataforma/index.php/alumnos/json?alumno_id=" + $("#datatable").DataTable().row( $(this).parents('tr') ).data()['alumno_id'],
          "dataType":"json"
        })
        .done(function(data) {
          if (data.length > 0) {
            $('#modal_retiro .modal-title').html('Retirar alumno <strong>' + data[0].alumno_nombres + ' ' + data[0].alumno_apellidop + '</strong>')
            $('#retiro_motivo').val(data[0].retiro_motivo).attr('alumno_id', data[0].alumno_id)
          }
        })
      }
    )

    $('.dataTables_length, .dataTables_filter, .dataTables_info, .dataTables_paginate').css('padding', '0')
    $('.dataTables_info, .dataTables_paginate').addClass('col-sm-6')
    $('.dataTables_length').addClass('col-sm-9')
    $('.dataTables_filter').addClass('col-sm-3').css('float', 'right').css('width', 'auto')
    $('.dataTables_paginate').css('float', 'right').css('width', 'auto')
    
    $('#curso_alumnos').append('<button data-toggle="modal" data-target="#modal_alumnos" class="btn btn-alumno btn-primary"><i class="fa fa-plus"></i> Agregar alumnos</button>')

    $('[data-toggle="tooltip"]').tooltip()
    $('#datatable').css('width', '100%')
    
    $('[data-title="tooltip"]').tooltip()
    
    $('.progress').fadeOut()
  }
</script>