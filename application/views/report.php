<html lang="es">
<head>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
  <meta charset="utf-8">
  <title>Plataforma | intAR21</title>
  <!-- Bootstrap Core CSS -->
  <link href="<?=base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
  <style type="text/css">
    body {
      background-color: #f4f4f4;
      color: #4F5155;
      font-family: 'open sans', helvetica;
      padding: 20px;
    }
    td:nth-child(2), td:nth-child(3), th:nth-child(2), th:nth-child(3) {
      text-align: center;
    }
    h1 {
      margin-top: 0px;
    }
  </style>
<style type="text/css" media="print">
   .no-print { display: none; }
</style>
</head>
<body>
  <div id="container">
<?php
  $usuario = $this->db->query('select * from usuario where usuario_id = '.$this->session->userdata('usuario_id'))->result()[0];
?>
    
    <h3>Estimado/a: <?=$usuario->usuario_nombre.' '.$usuario->usuario_appat?></h3>
    <p>
        El alumno/a <b><?=$alumno->result()[0]->nombre?></b>
        cuyo RUN es el <b><?=$alumno->result()[0]->rut?></b>
        correspondiente al curso <b><?=$alumno->result()[0]->curso?></b>
        cuyo docente encargado es <b><?=$alumno->result()[0]->docente?></b>
        del periodo escolar <b><?=$alumno->result()[0]->anio?></b>,
        ha realizado las siguientes actividades, consiguiendo los siguientes resultados:
    </p>
    <table class="table">
      <thead>
        <tr>
          <th>Aplicacion</th>
          <th>Actividad</th>
          <th>Porcentaje logrado</th>
          <th>Nivel</th>
        </tr>
      </thead>
      <tbody>
        <?php 
          foreach($resultados->result() as $row)
          {
            if ($row->percent > 100) { $row->percent = 100; }
            if ($row->percent == 0) { $row->percent = '0'; }
            echo '<tr><td>'.$row->aplicacion_nombre.'</td><td>'.$row->actividad_numero.'</td><td>'.$row->percent.' %</td><td>'.$row->nivel.'</td></tr>';
          }
        ?>
      </tbody>
    </table>
    <?php 
      $aux = true;
      foreach($resultados->result() as $row)
      {
        if ($row->nivel != 'Logrado') { 
          if ($aux) { 
            echo '<h4>El alumno ha de reforzar lo siguiente:</h4><ul>'; $aux = !$aux;
          }
          echo '<li>'.$row->actividad_nombre.'</li>';
        }
      }
      if (!$aux) { 
        echo '</ul>'; 
      }
    ?>
    <h4>Comentarios:</h4>
    <textarea style="width:100%"></textarea>
    <br><br>
  </div>
  <a type="button" class="btn btn-default no-print" style="float:right" onclick="javascript:download_pdf()">Descargar</a>
  <a type="button" class="btn btn-default no-print" style="float:right;margin-right:10px" onclick="window.print()">Imprimir</a>
  <br>
</body>
  <script src="<?=base_url('assets/js/jsPDF/jspdf.js')?>"></script>
  <script src="<?=base_url('assets/js/jsPDF/plugins/from_html.js')?>"></script>
  <script src="<?=base_url('assets/js/jsPDF/plugins/split_text_to_size.js')?>"></script>
  <script src="<?=base_url('assets/js/jsPDF/plugins/standard_fonts_metrics.js')?>"></script>
  <script src="<?=base_url('assets/js/jsPDF/plugins/cell.js')?>"></script>
  <script src="<?=base_url('assets/js/jsPDF/plugins/prevent_scale_to_fit.js')?>"></script>
  <script src="<?=base_url('assets/js/jsPDF/plugins/acroform.js')?>"></script>
  <script>
    function download_pdf() {
      var doc = new jsPDF();          
      var elementHandler = {
        '#ignorePDF': function (element, renderer) {
          return true;
        }
      };
      var source = window.document.getElementById("container");
      doc.fromHTML(
          source,
          16,
          8,
          {
            'width': 175,'elementHandlers': elementHandler
          });
      doc.setLineWidth(1).setFontSize(8);

      doc.output("dataurlnewwindow");
    }
  </script>
  
</html>