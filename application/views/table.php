<script type="text/javascript" charset="utf8" src="<?=base_url('assets/datatable/js/jquery.dataTables.js')?>"></script>
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/datatable/js/dataTables.buttons.min.js')?>"></script>
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/datatable/js/jszip.min.js')?>"></script>
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/datatable/js/buttons.html5.min.js')?>"></script>
<script type="text/javascript" charset="utf8" src="<?=base_url('assets/datatable/js/dataTables.bootstrap.min.js')?>"></script>

<link href="<?=base_url('assets/datatable/css/dataTables.bootstrap.css')?>" rel="stylesheet">

<style>
  tbody td:first-child {
    border-left: 1px solid #fff ;
  }
  tbody td:last-child {
    border-right: 1px solid #fff ;
    text-align: right;
  }
  tbody tr:last-child td {
    border-bottom: 1px solid #fff ;
  }
  #datatable tbody td {
    border-color: #e7edf5 ;
    padding: 5px 8px;
  }
  #datatable thead th {
    background-color: #e7edf5;
    border-color: #4a72b2 ;
    color: #4a72b2;
  }
  #datatable .btn {
    padding: 4px 12px;
  }
  #datatable .btn-edit, #datatable .btn-chart, #datatable .btn-report {
    color: #4a72b2;
  }
  #datatable {
    padding: 15px 0px;
  }
  .table-hover>tbody>tr:hover {
    background-color: #eff3f8;
    color: #4a72b2;
  }
  .dataTables_length label {
    padding-right: 8px;
  }
  .dataTables_length select {
    width: auto !important;
  }
  .center {
    text-align: center;
  }
</style>

<h1>
  <?=$title?>
  <?php if ($this->uri->segment(1) != 'actividades') { ?>
  <a class="btn btn-primary" href="/plataforma/index.php/<?=explode('?', $resrc)[0]?>/editar" style="position: absolute; right: 190px;"><i class="fa fa-user-plus"></i> Agregar </a>
   <?php } ?>
</h1>
<hr>
<table class="table table-hover" id="datatable" data-source="<?=$resrc?>">
  <thead></thead>
  <tbody></tbody>
</table>

<script type="text/javascript" charset="utf8">
  $(document).ready(function() {
    var json_name = $('#datatable').attr('data-source').split('?')[0], json_data = '?' + $('#datatable').attr('data-source').split('?')[1]
    
    if (json_data == '?undefined') { json_data = '' }
    
    $.getJSON('/plataforma/assets/json/' + json_name + '.datatable.json', function(data) {
      $('#datatable thead').append('<tr></tr>')

      var buttonsGroup = '';

      $.each(data.buttons, function(i, e) {
          if (e.access >= <?=$this->session->userdata('tipo_id')?>) {
              buttonsGroup = buttonsGroup + "<button style='margin-left:-1px' data-toggle='tooltip' data-placement='top' class='btn " + e.class + "' title='" + e.title + "'><i class='" + e.icon + "'></i></button>"
          }
      });

      var aux = data.columns.filter(function(e) {
          if (e.access >= <?=$this->session->userdata('tipo_id')?>) {
              return e;
          }
      });
         
      data.columns = aux;
          
      aux = data.columns.filter(function(e) {
        if (e.data == 'alumno_edad') {
          e['render'] = function(data, type, row, meta) { return data + " años" }
        }

        return e
      });
          
      data.columns = aux;
      
      $.each(data.columns, function(i, e) {
        $('#datatable thead tr').append('<th></th>')

      });

      data.columns.push({ "targets":-1, "data": null, "defaultContent":buttonsGroup, "orderable": false })
      $('#datatable thead tr').append('<th></th>');
      
      $('#datatable')
      .DataTable({        
        "ajax":
        {
          "url":"/plataforma/index.php/" + json_name + "/json" + json_data,
          "type":"GET",
          "dataSrc":""
        },
        "buttons":
        [
          {
            "extend":"excelHtml5",
            "exportOptions":
            {
              "columns":":visible"
            }
          }
        ],
        "columns":data.columns,
        "columnDefs":
        [
          {
            "width":"10%",
            "targets":1
          }
        ],
        "dom":"Blfrtip",
        "initComplete":function() 
        { 
          render(data.filters)
        },
        "language":
        {
          "lengthMenu": "<strong>Mostrar </strong> _MENU_",
          "info":"Mostrando _START_ hasta _END_ de _TOTAL_ ",
          "search":"<strong>Filtrar </strong> _INPUT_",
          "infoFiltered":"(filtrado de _MAX_ )",
          "zeroRecords": "Sin resultados ",
          "paginate":
          {
            "previous":"Anterior",
            "next":"Siguiente"
          }
        }
      })
    })    
  })

  function render(filters)
  {
    $("#datatable tbody")
      .on(
      'click',
      'button.btn-chart',
      function()
      {
        var data = $("#datatable").DataTable().row( $(this).parents('tr') ).data()
        window.location = "/plataforma/index.php/resultados?" + Object.keys(data)[0] + '=' + data[Object.keys(data)[0]]
      }
    )
      .on(
      'click',
      'button.btn-edit',
      function()
      {
        var data = $("#datatable").DataTable().row( $(this).parents('tr') ).data()
        window.location = "/plataforma/index.php/" + $('#datatable').attr('data-source').split('?')[0] + '/editar/' + data[Object.keys(data)[0]]
      }
    )
      .on(
      'click',
      'button.btn-report',
      function()
      {
        var data = $("#datatable").DataTable().row( $(this).parents('tr') ).data()
        window.open("/plataforma/index.php/" + $('#datatable').attr('data-source').split('?')[0] + '/reporte/' + data[Object.keys(data)[0]], '_blank')
      }
    )
    
    $.each(filters, function(i, e) {
      if (e.access >= <?=$this->session->userdata('tipo_id')?>) 
      { 
        var column = $('#datatable').DataTable().column(e.data)
        var select = $('<select class="form-control input-sm"><option value=""></option></select>')

          .on( 'change', function () {
            var val = $.fn.dataTable.util.escapeRegex(
              $(this).val()
            )

            column.search( val ? '^'+val+'$' : '', true, false ).draw()
          })

        var label = $('<label><strong>' + $(column.header()).text() + ' </strong></label>').append(select).appendTo( '.dataTables_length' )

        column.data().unique().sort().each( function ( d, j ) {
          select.append( '<option value="'+d+'">'+d+'</option>' )
        })
      }
    })

    $('.dataTables_length, .dataTables_filter, .dataTables_info, .dataTables_paginate').css('padding', '0')
    $('.dataTables_info, .dataTables_paginate').addClass('col-sm-6')
    $('.dataTables_length').addClass('col-sm-9')
    $('.dataTables_filter').addClass('col-sm-3')

    $('.buttons-excel')
      .appendTo('h1')
      .text('')
      .addClass('btn btn-success')
      .css('position', 'absolute')
      .css('right', '15')
      .append('<i class="fa fa-file-excel-o" aria-hidden="true"></i> Descargar en Excel')

    $('[data-toggle="tooltip"]').tooltip()
    $('#datatable').css('width', '100%')
    $('.progress').fadeOut()
  }
</script>
