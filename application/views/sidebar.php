<style>
  #sidebar-wrapper h4 {
    color: white;
    text-align: center;
  }
</style>

<!-- Sidebar -->
<div id="sidebar-wrapper">
  <ul class="sidebar-nav">
    <li class="btn-black">
      <a id="menu-toggle" href="#">
        <i class="fa fa-bars"></i>Menú
      </a>
    </li>
  </ul>
  
  <div class="avatar admin">
    <a href="/plataforma/index.php/usuarios/editar/<?=$this->session->userdata('usuario_id')?>">
      <img src="<?=$this->session->userdata('usuario_imagen')?>" class="img-circle img-thumbnail">
    </a>
    
    <div class="avatar institution">
      <a href="/plataforma/index.php/instituciones/editar/<?=$this->session->userdata('institucion_id')?>">
        <img src="<?=$this->session->userdata('institucion_imagen')?>" class="img-circle img-thumbnail">
      </a>
    </div>
  </div>
  
  <h4><?=$this->session->userdata('usuario_nombre')?></h4>
  
  <ul class="sidebar-nav">
    <br>
    <li>
      <a href="/plataforma/index.php/bienvenido" <?php if ($this->router->fetch_class() == 'bienvenido') { echo 'class="active"'; } ?>>
        <i class="fa fa-home"></i>Inicio
      </a>
    </li>
    
    <?php if ($this->session->userdata('tipo_id') < 3) { ?>
    <li>
      <a data-toggle="collapse" data-target="#usuarios">
        <?php if ($this->session->userdata('tipo_id') < 2) { echo '<i class="fa fa-user"></i>Usuarios'; } else { echo '<i class="fa fa-graduation-cap"></i>Docentes'; } ?>
      </a>
      
      <div id="usuarios" class="collapse">
        <a href="/plataforma/index.php/usuarios/editar">
          <i class="fa fa-user-plus"></i><?php if ($this->session->userdata('tipo_id') < 2) { echo 'Agregar usuarios'; } else { echo 'Agregar docentes'; } ?>
        </a>
        
        <a href="/plataforma/index.php/usuarios">
          <i class="fa fa-table"></i><?php if ($this->session->userdata('tipo_id') < 2) { echo 'Gestion de usuarios'; } else { echo 'Gestion de Docentes'; } ?>
        </a>
      </div>
    </li>
    <?php } ?>
    
    <li>
      <a data-toggle="collapse" data-target="#alumnos">
        <i class="fa fa-child"></i>Alumnos
      </a>
      
      <div id="alumnos" class="collapse">
        <a href="/plataforma/index.php/alumnos/editar"><i class="fa fa-user-plus"></i>Agregar alumno</a>
        <a href="/plataforma/index.php/alumnos"><i class="fa fa-table"></i>Gestion de  alumnos</a>
      </div>
    </li>
    
    <li>
      <a href="/plataforma/index.php/cursos">
        <i class="fa fa-users"></i>cursos
      </a>
    </li>
    
    <li>
      <a href="/plataforma/index.php/grupos">
        <i class="fa fa-smile-o"></i>grupos pie
      </a>
    </li>
    
    <li>
      <a href="/plataforma/index.php/actividades">
        <i class="fa fa-check"></i>Actividades
      </a>
    </li>
    
    <li>
      <a href="/plataforma/index.php/ayuda">
        <i class="fa fa-question"></i>Ayuda
      </a>
    </li>
    
    <li class="btn-danger">
      <a href="/plataforma/index.php/usuarios/logout">
        <i class="fa fa-times"></i>Cerrar Sesión
      </a>
    </li>
  </ul>
</div>
<!-- /#sidebar-wrapper -->

<script>
  $(document).ready(function() {
    $('a[href$="/<?=uri_string()?>"]').addClass('active').parent('.collapse').addClass('in')
    
    $('li a').click(function() {
      $('.collapse.in').collapse('hide')
    })
  })
</script>
