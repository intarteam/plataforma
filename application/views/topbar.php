<style>
  .breadcrumb {
    background-color: #383f48;
    border-radius: 0px;
    color: #aabddc;
    top: 0px;
    margin-left: -30px;
    padding: 10px;
    position:fixed;
    width: 100%;
    z-index:500;
  }
</style>
<ol class="breadcrumb">
  <li><a href="/plataforma"><i class="fa fa-home"></i></a></li>
  <?php
  
    $class = $this->router->fetch_class();
  
    if ($class == 'usuarios' and $this->session->userdata('tipo_id') > 1) {
      $class = 'docentes';
    }
  
    if ($this->router->fetch_method() != 'index') { echo '<li><a href="/plataforma/index.php/'.$this->router->fetch_class().'">'.ucfirst($class).'</a></li>'; }

  ?>
  <li><?=$title?></li>
</ol>