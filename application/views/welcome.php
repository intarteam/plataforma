<style>
  .welcome-header {
    background-image: url('/plataforma/assets/images/PortadaDocentes.jpg');
    background-position: 50% 50%;
    background-size: cover;
    margin: -20px -30px;
    min-height: 60%;
    text-align: right;
    padding: 10px 30px;
  }
  
  .welcome-header h1 {
    color: #fff;
    font-size: 50pt;
  }
  
  .panel-primary > .panel-heading {
    background-color: #0085ff;
    border-color: #0085ff;
  }
  
  .panel-green > .panel-heading {
    background-color: #009aa3;
    color: #fff;
  }
  
  .panel-green {
    border-color: #009aa3;
  }
  
  .panel-yellow > .panel-heading {
    background-color: #a59e6a;
    color: #fff;
  }
  
  .panel-yellow {
    border-color: #a59e6a;
  }

  .panel-red > .panel-heading {
    background-color: #ab5c62;
    color: #fff;
  }
  
  .panel-red {
    border-color: #ab5c62;
  }
  
  .huge {
    font-size: 40pt;
  }
</style>

<div class="welcome-header">
  <h1>Bienvenido</h1>
</div>

<br><br>

<div class="row">
  <div class="col-lg-3 col-md-6">
      <div class="panel panel-primary">
          <div class="panel-heading">
              <div class="row">
                  <div class="col-xs-3">
                      <i class="fa fa-5x"></i>
                  </div>
                  <div class="col-xs-9 text-right">
                      <div class="huge"></div>
                      <div style="font-size: 14pt;">Instituciones</div>
                  </div>
              </div>
          </div>
          <a href="#">
              <div class="panel-footer">
                  <span class="pull-left">Ver Detalles</span>
                  <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                  <div class="clearfix"></div>
              </div>
          </a>
      </div>
  </div>
  <div class="col-lg-3 col-md-6">
      <div class="panel panel-green">
          <div class="panel-heading">
              <div class="row">
                  <div class="col-xs-3">
                      <i class="fa fa-5x"></i>
                  </div>
                  <div class="col-xs-9 text-right">
                      <div class="huge">12</div>
                      <div style="font-size: 14pt;">Usuarios</div>
                  </div>
              </div>
          </div>
          <a href="#">
              <div class="panel-footer">
                  <span class="pull-left">Ver Detalles</span>
                  <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                  <div class="clearfix"></div>
              </div>
          </a>
      </div>
  </div>
  <div class="col-lg-3 col-md-6">
      <div class="panel panel-yellow">
          <div class="panel-heading">
              <div class="row">
                  <div class="col-xs-3">
                      <i class="fa fa-5x"></i>
                  </div>
                  <div class="col-xs-9 text-right">
                      <div class="huge">124</div>
                      <div style="font-size: 14pt;">Alumnos</div>
                  </div>
              </div>
          </div>
          <a href="#">
              <div class="panel-footer">
                  <span class="pull-left">Ver Detalles</span>
                  <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                  <div class="clearfix"></div>
              </div>
          </a>
      </div>
  </div>
  <div class="col-lg-3 col-md-6">
      <div class="panel panel-red">
          <div class="panel-heading">
              <div class="row">
                  <div class="col-xs-3">
                      <i class="fa fa-5x"></i>
                  </div>
                  <div class="col-xs-9 text-right">
                      <div class="huge">13</div>
                      <div style="font-size: 14pt;">Aplicaciones</div>
                  </div>
              </div>
          </div>
          <a href="#">
              <div class="panel-footer">
                  <span class="pull-left">Ver Detalles</span>
                  <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                  <div class="clearfix"></div>
              </div>
          </a>
      </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    <?php if ($this->session->userdata('tipo_id') == 1) { ?>
    $.getJSON('/plataforma/index.php/instituciones/json', function(data) {
      $('.panel.panel-primary .huge').text(data.length).next().text('Instituciones')
      $('.panel.panel-primary a').attr('href', '/plataforma/index.php/instituciones')
      $('.panel.panel-primary .panel-heading i').addClass('fa-university')
    })
    $.getJSON('/plataforma/index.php/usuarios/json', function(data) {
      $('.panel.panel-green .huge').text(data.length).next().text('Usuarios')
      $('.panel.panel-green a').attr('href', '/plataforma/index.php/usuarios')
      $('.panel.panel-green .panel-heading i').addClass('fa-user')
    })
    $.getJSON('/plataforma/index.php/cursos/json', function(data) {
      $('.panel.panel-yellow .huge').text(data.length).next().text('Cursos')
      $('.panel.panel-yellow a').attr('href', '/plataforma/index.php/cursos')
      $('.panel.panel-yellow .panel-heading i').addClass('fa-users')
      
    })
    $.getJSON('/plataforma/index.php/grupos/json', function(data) {
      $('.panel.panel-red .huge').text(data.length).next().text('Grupos')
      $('.panel.panel-red a').attr('href', '/plataforma/index.php/grupos')
      $('.panel.panel-red .panel-heading i').addClass('fa-smile-o')
    })
    
    <?php } elseif ($this->session->userdata('tipo_id') == 2) { ?>
    $.getJSON('/plataforma/index.php/usuarios/json?tipo_id=3', function(data) {
      $('.panel.panel-primary .huge').text(data.length).next().text('Docentes')
      $('.panel.panel-primary a').attr('href', '/plataforma/index.php/usuarios')
      $('.panel.panel-primary .panel-heading i').addClass('fa-graduation-cap')
    })
    $.getJSON('/plataforma/index.php/cursos/json', function(data) {
      $('.panel.panel-green .huge').text(data.length).next().text('Cursos')
      $('.panel.panel-green a').attr('href', '/plataforma/index.php/cursos')
      $('.panel.panel-green .panel-heading i').addClass('fa-users')
    })
    $.getJSON('/plataforma/index.php/grupos/json', function(data) {
      $('.panel.panel-yellow .huge').text(data.length).next().text('Grupos')
      $('.panel.panel-yellow a').attr('href', '/plataforma/index.php/grupos')
      $('.panel.panel-yellow .panel-heading i').addClass('fa-smile-o')
    })
    $.getJSON('/plataforma/index.php/alumnos/json', function(data) {
      $('.panel.panel-red .huge').text(data.length).next().text('Alumnos')
      $('.panel.panel-red a').attr('href', '/plataforma/index.php/alumnos')
      $('.panel.panel-red .panel-heading i').addClass('fa-child')
    })
    
    <?php } else { ?>
    $.getJSON('/plataforma/index.php/cursos/json', function(data) {
      $('.panel.panel-primary .huge').text(data.length).next().text('Cursos')
      $('.panel.panel-primary a').attr('href', '/plataforma/index.php/cursos')
      $('.panel.panel-primary .panel-heading i').addClass('fa-users')
    })
    $.getJSON('/plataforma/index.php/grupos/json', function(data) {
      $('.panel.panel-green .huge').text(data.length).next().text('Grupos')
      $('.panel.panel-green a').attr('href', '/plataforma/index.php/grupos')
      $('.panel.panel-green .panel-heading i').addClass('fa-smile-o')
    })
    $.getJSON('/plataforma/index.php/alumnos/json', function(data) {
      $('.panel.panel-yellow .huge').text(data.length).next().text('Alumnos')
      $('.panel.panel-yellow a').attr('href', '/plataforma/index.php/alumnos')
      $('.panel.panel-yellow .panel-heading i').addClass('fa-child')
    })
    $.getJSON('/plataforma/index.php/actividades/json', function(data) {
      $('.panel.panel-red .huge').text(data.length).next().text('Actividades')
      $('.panel.panel-red a').attr('href', '/plataforma/index.php/actividades')
      $('.panel.panel-red .panel-heading i').addClass('fa-gamepad')
      
    })
    <?php } ?>
    
    $('.progress').fadeOut()
  })
</script>