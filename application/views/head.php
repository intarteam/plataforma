<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?=$title?> | intAR21</title>

  <!-- Bootstrap Core CSS -->
  <link href="<?=base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">

  <!-- Custom CSS -->
  <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet'>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
  <link href="/plataforma/assets/css/dashboard.css" rel="stylesheet">
  <link href="<?=base_url("assets/css/button.css")?>" rel="stylesheet">
  <link href="<?=base_url("assets/css/avatar.css")?>" rel="stylesheet">
  
  <link href="/plataforma/assets/datatable/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
  <link href="/plataforma/assets/css/jquery.growl.css" rel="stylesheet" type="text/css">
  
  <link rel="shortcut icon" href="/plataforma/assets/images/favicon.png" type="image/x-icon">
  
  <link rel="stylesheet" href="/plataforma/assets/css/jquery-ui.css">

  <!-- jQuery -->
  <script src="<?=base_url('assets/js/jquery.js')?>"></script>
  <script>
		function nombre_funcion($id){
			var obj = document.getElementById($id);
			if (obj.style.display == "none") obj.style.display= "block"; // un string vacio o "block";
			else obj.style.display= "none";
		}
  </script>
  <style type="text/css">
  #PunteroAqui:hover{ 
	width:230px; 
	height:300px; 
	position:absolute;
	border: 1px solid black;
	background-color: #CECEF6;
	box-shadow: 4px 4px 5px grey;
    

	}
  #PunteroAqui{
	width:20px; 
	height:20px; 
	overflow:hidden; 
	position:absolute; 

	}
  </style>
</head>