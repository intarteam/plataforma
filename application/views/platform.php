<html lang="es">
  <?php $this->load->view('head') ?>
  <body>
    <div id="wrapper">
      <?php $this->load->view('sidebar') ?>
      
      <!-- Page Content -->
      <div id="page-content-wrapper">
        <div class="container-fluid">
          <div class="row" style="margin-top:30px">
            <div id="print" class="col-lg-12">
              <link href="<?=base_url('assets/css/loading.css')?>" rel="stylesheet">
              <div class="progress">
                <div class="indeterminate"></div>
              </div>
              <?php $this->load->view('topbar'); ?>
              <?php $this->load->view($paper); ?>
            </div>
          </div>
        </div>
      </div>
      <!-- /#page-content-wrapper -->
      
    </div>
    <!-- /#wrapper -->

    <!-- Bootstrap Core JavaScript -->
    <script src="<?=base_url('assets/js/bootstrap.min.js')?>"></script>
    
    <!-- Menu Toggle Script -->
    <script>
      $("#menu-toggle").click(function(e) {
          e.preventDefault()
          $("#wrapper").toggleClass("toggled")
      })
    </script>
  </body>
</html>
