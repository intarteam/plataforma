<?php 

  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

  class Institucion extends CI_Model 
  {
    public function __construct()
    {
      parent::__construct();
    }

    function push($post)
    {    
      if ($this->session->userdata('typ') > 2 and ($post['institucion_id'] != $this->session->userdata('iid') and $this->session->userdata('typ') != 1)) {
        http_response_code(403); 
        echo json_encode(['title'=>'Ouch!', 'message'=>'La solicitud fue legal, pero el sistema rehúsa responderla dado que usted no tiene los privilegios para hacerla.']); 
        return; 
      }

      if ($this->db->insert('institucion', $post)) {
        http_response_code(200); 
        echo json_encode(['title'=>'Yeah!', 'message'=>"La institucion ".$post['institucion_nombre']." fue agregado."]); 
        return; 
      }

      http_response_code(400); 
      echo json_encode(['title'=>'Ouch!', 'message'=>'Datos corruptos.']); 
      return;
    }

    function edit($patch)
    {
      if (!$this->seek(['institucion_id' => $this->input->get('institucion_id')])) { 
        http_response_code(404); 
        echo json_encode(['title'=>'Ouch!', 'message'=>'Permiso de edicion denegado.']); 
        return;
      }

      if ($this->db->update('institucion', $patch, "institucion_id = ".$this->input->get('institucion_id'))) {
        $institucion = $this->seek(['institucion_id' => $this->input->get('institucion_id')])[0];
        
        http_response_code(200); 
        echo json_encode(['title'=>'Yeah!', 'message'=>"Los datos de ".$institucion->institucion_nombre." se han actualizado."]); 
        return; 
      }

      http_response_code(400); 
      echo json_encode(['title'=>'Ouch!', 'message'=>'Datos corruptos.']); 
      return;
    }

    function seek($where)
    {
      if (isset($where['_'])) { unset($where['_']); }

      $fields = 'institucion.*';

      if ($this->session->userdata('typ') < 2) 
      {
        $fields .= ', tipo_subvencion.tiposub_nombre';
        $this->db->join('tipo_subvencion', 'institucion.tiposub_id = tipo_subvencion.tiposub_id');

        $fields .= ', comuna.comuna_nombre';
        $this->db->join('comuna', 'institucion.comuna_id = comuna.comuna_id');
      }

      else
      {
        $this->db->where('institucion_id = '.$this->session->userdata('iid'));
      }

      $this->db->select($fields);
      $this->db->from('institucion');
      $this->db->where($where);

      return $this->db->get()->result();
    }
  }