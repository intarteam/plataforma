<?php 
  
  if (!defined('BASEPATH')) exit('No direct script access allowed'); 

  class Usuario extends CI_Model 
  {

    public function __construct()
    {
      parent::__construct();
    }

    function push($post)
    {    
      if ($this->session->userdata('typ') > 2 and ($post['institucion_id'] != $this->session->userdata('iid') and $this->session->userdata('typ') != 1)) {
        http_response_code(403); 
        echo json_encode(['title'=>'Ouch!', 'message'=>'La solicitud fue legal, pero el sistema rehúsa responderla dado que usted no tiene los privilegios para hacerla.']); 
        return; 
      }

      $post['usuario_clave'] = md5($post['usuario_clave']); 

      if ($this->db->insert('usuario', $post)) {
        http_response_code(200); 
        echo json_encode(['title'=>'Yeah!', 'message'=>"El usuario ".$post['usuario_nombre']." ".$post['usuario_appat']." fue agregado."]); 
        return; 
      }

      http_response_code(400); 
      echo json_encode(['title'=>'Ouch!', 'message'=>'Datos corruptos.']); 
      return;
    }

    function edit($patch)
    {
      if (!$this->seek(['usuario_id' => $this->input->get('usuario_id')])) { 
        http_response_code(404); 
        echo json_encode(['title'=>'Ouch!', 'message'=>'Permiso de edicion denegado.']); 
        return;
      }

      if (!$patch['usuario_clave']) { 
        unset($patch['usuario_clave']); 
      } else { 
        $patch['usuario_clave'] = md5($patch['usuario_clave']); 
      }

      if ($this->db->update('usuario', $patch, "usuario_id = ".$this->input->get('usuario_id'))) {
        $this->session->set_userdata('nom', $patch['usuario_nombre']." ".$patch['usuario_appat']);

        http_response_code(200); 
        echo json_encode(['title'=>'Yeah!', 'message'=>"Los datos de ".$patch['usuario_nombre']." ".$patch['usuario_appat']." se han actualizado."]); 
        return; 
      }

      http_response_code(400); 
      echo json_encode(['title'=>'Ouch!', 'message'=>'Datos corruptos.']); 
      return;
    }

    function seek($where)
    {
      if (isset($where['_'])) { unset($where['_']); }
      
      if (isset($where['tipo_id'])) { $where['usuario.tipo_id'] = $where['tipo_id']; unset($where['tipo_id']); }

      $fields = 'usuario.*, concat(usuario_nombre, " ", usuario_appat, " ", usuario_apmat) as usuario_nombre_completo, institucion.institucion_imagen, institucion.institucion_nombre, tipo_usuario.tipo_nombre';
      
      if ($this->session->userdata('tipo_id') > 1) 
      {
        $this->db->where('usuario.institucion_id = '.$this->session->userdata('institucion_id'));
        $this->db->where('(usuario.tipo_id > '.$this->session->userdata('tipo_id').' OR usuario_id = '.$this->session->userdata('usuario_id').')');
      }
      
      $this->db->join('institucion', 'usuario.institucion_id = institucion.institucion_id');
      $this->db->join('tipo_usuario', 'usuario.tipo_id = tipo_usuario.tipo_id');

      $this->db->select($fields);
      $this->db->from('usuario');
      $this->db->where($where);

      return $this->db->get()->result();
    }
  }