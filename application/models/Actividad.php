<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); class Actividad extends CI_Model {

  public function __construct()
  {
    parent::__construct();
  }
  
    function push($post)
    {
      if ($this->session->userdata('tipo_id') > 1) {
        http_response_code(403); 
        echo json_encode(['title'=>'Ouch!', 'message'=>'La solicitud fue legal, pero el sistema rehúsa responderla dado que usted no tiene los privilegios para hacerla.']); 
        return false; 
      }
      
      if ($this->db->insert('actividad', $post)) {
        http_response_code(200); 
        echo json_encode(['title'=>'Yeah!', 'message'=>"El alumno ".$post['alumno_nombres']." fue agregado."]); 
        return true; 
      }

      http_response_code(400); 
      echo json_encode(['title'=>'Ouch!', 'message'=>'Datos corruptos.']); 
      return false;
    }
    
    function edit($patch)
    {
      if ($this->session->userdata('tipo_id') > 1) { 
        http_response_code(404); 
        echo json_encode(['title'=>'Ouch!', 'message'=>'Permiso de edicion denegado.']); 
        return;
      }

      if ($this->db->update('actividad', $patch, "actividad_id = ".$this->input->get('actividad_id'))) {
        http_response_code(200); 
        echo json_encode(['title'=>'Yeah!', 'message'=>"Los datos de la Actividad ".$patch['actividad_numero']." se han actualizado."]); 
        return; 
      }

      http_response_code(400); 
      echo json_encode(['title'=>'Ouch!', 'message'=>'Datos corruptos.']); 
      return;
    }
  
    function seek($where)
    {
      if (isset($where['_'])) { unset($where['_']); }

      $fields = 'actividad.*, aplicacion.aplicacion_nombre';
      
      $this->db->join('aplicacion', 'aplicacion.aplicacion_id = actividad.aplicacion_id');

      $this->db->select($fields);
      $this->db->from('actividad');
      $this->db->where($where);

      return $this->db->get()->result();
    }
}