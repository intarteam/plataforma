<?php

  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

  class Curso extends CI_Model {

    public function __construct()
    {
      parent::__construct();
    }

    function push($post)
    { 
      if ($this->session->userdata('tipo_id') > 2) {
        http_response_code(403); 
        echo json_encode(['title'=>'Ouch!', 'message'=>'La solicitud fue legal, pero el sistema rehúsa responderla dado que usted no tiene los privilegios para hacerla.']); 
        return false; 
      }

      if (isset($post['curso_grado']) and $post['curso_grado'] == '') {
          $post['curso_grado'] = null;
      }
      
      if ($this->db->insert('curso', $post)) {
        http_response_code(200); 
        echo json_encode(['title'=>'Yeah!', 'message'=>"El curso ".$post['curso_grado'].$post['curso_letra']." fue agregado."]); 
        return true; 
      }

      http_response_code(400); 
      echo json_encode(['title'=>'Ouch!', 'message'=>'Datos corruptos.']); 
      return false;
    }
    
    function edit($patch)
    {
      if (!$this->seek(['curso_id' => $this->input->get('curso_id')])) { 
        http_response_code(404); 
        echo json_encode(['title'=>'Ouch!', 'message'=>'Permiso de edicion denegado.']); 
        return;
      }

        if (isset($patch['curso_grado']) and $patch['curso_grado'] == '') {
            $patch['curso_grado'] = null;
        }

      if ($this->db->update('curso', $patch, "curso_id = ".$this->input->get('curso_id'))) {
        $alumno = $this->seek(['curso_id' => $this->input->get('curso_id')])[0];
        
        http_response_code(200); 
        echo json_encode(['title'=>'Yeah!', 'message'=>"Los datos del curso ".$patch['curso_grado'].$patch['curso_letra']." se han actualizado."]); 
        return; 
      }

      http_response_code(400); 
      echo json_encode(['title'=>'Ouch!', 'message'=>'Datos corruptos.']); 
      return;
    }

    function seek($where)
    {
      if (isset($where['_'])) { unset($where['_']); }
      
      if (isset($where['curso_id'])) { $where['curso.curso_id'] = $where['curso_id']; unset($where['curso_id']); }

      $fields = 'curso.*, institucion_nombre, concat(usuario.usuario_nombre, " ", usuario.usuario_appat) as curso_docente, concat(coalesce(curso.curso_grado, ""), curso.curso_letra) as curso_grado_letra';
      
      if ($this->session->userdata('tipo_id') > 1) {
        $where['curso.institucion_id'] = $this->session->userdata('institucion_id');
      }
      
      if ($this->session->userdata('tipo_id') > 2) {
        $where['curso.usuario_id'] = $this->session->userdata('usuario_id');
      }
      
      $this->db->join('usuario', 'usuario.usuario_id = curso.usuario_id');
      $this->db->join('institucion', 'institucion.institucion_id = curso.institucion_id');

      $this->db->select($fields);
      $this->db->from('curso');
      $this->db->where($where);

      return $this->db->get()->result();
    }
}