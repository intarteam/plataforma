<?php 

  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

  class Alumno extends CI_Model {

    public function __construct()
    {
      parent::__construct();
    }

    function push($post)
    { 
      if ($this->db->get_where('alumno', ['alumno_rut' => $post['alumno_rut']])->result()) { 
        http_response_code(404); 
        echo json_encode(['title'=>'Ouch!', 'message'=>'El alumno ya esta registrado.']); 
        return false;
      }
      
      if ($this->session->userdata('tipo_id') > 1) {
        $post['institucion_id'] = $this->session->userdata('institucion_id');
      }

      if (isset($post['grupo_id']) and $post['grupo_id'] == 'Ninguno') {
          unset($post['grupo_id']);
      }
      
      if ($this->db->insert('alumno', $post)) {
        http_response_code(200); 
        echo json_encode(['title'=>'Yeah!', 'message'=>"El alumno ".$post['alumno_nombres']." fue agregado."]); 
        return true; 
      }

      http_response_code(400); 
      echo json_encode(['title'=>'Ouch!', 'message'=>'Datos corruptos.']); 
      return false;
    }
    
    function edit($patch)
    {
      if (!$this->seek(['alumno_id' => $this->input->get('alumno_id')])) { 
        http_response_code(404); 
        echo json_encode(['title'=>'Ouch!', 'message'=>'Permiso de edicion denegado.']); 
        return;
      }

        if (isset($patch['grupo_id']) and $patch['grupo_id'] == 'Ninguno') {
            $patch['grupo_id'] = null;
        }

      if ($this->db->update('alumno', $patch, "alumno_id = ".$this->input->get('alumno_id'))) {
        $alumno = $this->seek(['alumno_id' => $this->input->get('alumno_id')])[0];
        
        http_response_code(200); 
        echo json_encode(['title'=>'Yeah!', 'message'=>"Los datos de ".$alumno->alumno_nombres." se han actualizado."]); 
        return; 
      }

      http_response_code(400); 
      echo json_encode(['title'=>'Ouch!', 'message'=>'Datos corruptos.']); 
      return;
    }

    function seek($where)
    {
      if (isset($where['_'])) { unset($where['_']); }
      
      if (isset($where['alumno_id'])) { $where['alumno.alumno_id'] = $where['alumno_id']; unset($where['alumno_id']); }
      if (isset($where['curso_id'])) { 
        if ($where['curso_id'] == 'null') {
          $this->db->where('curso.curso_id is null');
        } else {
          $where['curso.curso_id'] = $where['curso_id']; 
        }
        unset($where['curso_id']); 
      }

      $fields = 'alumno.*, comuna.*, curso_alumno.cur_al_observacion, curso_alumno.retiro_motivo, coalesce(concat(coalesce(curso_grado, ""), curso_letra), "Sin curso") as curso_grado_letra, concat(coalesce(alumno.alumno_apellidop, ""), " ", coalesce(alumno.alumno_apellidom, "")) as alumno_apellidos, year(curdate()) - year(alumno.alumno_nacimiento) as alumno_edad';

      if ($this->session->userdata('tipo_id') < 2) 
      {
        $fields .= ', institucion.institucion_nombre';
        $this->db->join('institucion', 'alumno.institucion_id = institucion.institucion_id');
      }
      else
      {
        $this->db->where('alumno.institucion_id = '.$this->session->userdata('institucion_id'));

        if ($this->session->userdata('tipo_id') == 3)
        {
            $this->db->where('(curso.usuario_id = '.$this->session->userdata('usuario_id').' OR curso.usuario_id is null)');
        }
      } 
      
      $this->db->join('comuna', 'alumno.comuna_id = comuna.comuna_id');
      $this->db->join('curso_alumno', 'curso_alumno.alumno_id = alumno.alumno_id and Year(fecha_incorporacion) = '.date('Y'), 'left');
      $this->db->join('curso', 'curso_alumno.curso_id = curso.curso_id', 'left');
      
      $this->db->select($fields);
      $this->db->from('alumno');
      $this->db->where($where);
      $this->db->where('(Year(fecha_incorporacion) = '.date('Y').' OR fecha_incorporacion is null)');
      
      return $this->db->get()->result();
    }
  }