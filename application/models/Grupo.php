<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); class Grupo extends CI_Model {

  public function __construct()
  {
    parent::__construct();
  }
  
    function push($post)
    { 
      if ($this->session->userdata('tipo_id') > 2) {
        http_response_code(403); 
        echo json_encode(['title'=>'Ouch!', 'message'=>'La solicitud fue legal, pero el sistema rehúsa responderla dado que usted no tiene los privilegios para hacerla.']); 
        return false; 
      }
      
      if ($this->db->insert('grupo', $post)) {
        http_response_code(200); 
        echo json_encode(['title'=>'Yeah!', 'message'=>"El grupo ".$post['grupo_identificador']." fue agregado."]); 
        return true; 
      }

      http_response_code(400); 
      echo json_encode(['title'=>'Ouch!', 'message'=>'Datos corruptos.']); 
      return false;
    }
    
    function edit($patch)
    {
      if (!$this->seek(['grupo_id' => $this->input->get('grupo_id')])) { 
        http_response_code(404); 
        echo json_encode(['title'=>'Ouch!', 'message'=>'Permiso de edicion denegado.']); 
        return;
      }

      if ($this->db->update('grupo', $patch, "grupo_id = ".$this->input->get('grupo_id'))) {
        $alumno = $this->seek(['grupo_id' => $this->input->get('grupo_id')])[0];
        
        http_response_code(200); 
        echo json_encode(['title'=>'Yeah!', 'message'=>"Los datos del grupo ".$patch['grupo_identificador']." se han actualizado."]); 
        return; 
      }

      http_response_code(400); 
      echo json_encode(['title'=>'Ouch!', 'message'=>'Datos corruptos.']); 
      return;
    }
  
    function seek($where)
    {
      if (isset($where['_'])) { unset($where['_']); }

      $fields = 'grupo.*, institucion.institucion_nombre as grupo_institucion';
      
      if ($this->session->userdata('tipo_id') > 1) {
        $where['institucion.institucion_id'] = $this->session->userdata('institucion_id');
      }
      
      $this->db->join('institucion', 'institucion.institucion_id = grupo.institucion_id');

      $this->db->select($fields);
      $this->db->from('grupo');
      $this->db->where($where);

      return $this->db->get()->result();
    }
}