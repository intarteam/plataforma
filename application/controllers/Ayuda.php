<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ayuda extends CI_Controller {
  
  function __construct() 
  {
    parent::__construct();
    if (!$this->session->userdata('usuario_id')) 
    { 
      redirect('ingresar', 'refresh'); 
    }
    
    if (isset($_GET['_'])) 
    {
      unset($_GET['_']);
    }
  }
  
  public function index()
  {
    $data['title'] = 'Ayuda';
    $data['paper'] = 'article';
    
    $this->load->view('platform', $data);
  }
}
