<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  class Grupos extends CI_Controller {

    function __construct() 
    {
      parent::__construct();
      if (!$this->session->userdata('usuario_id')) { redirect('ingresar', 'refresh'); }
    }

    public function index()
    {
      switch($this->input->method()) 
      {
        case 'post':
          $this->load->model('grupo');
          return $this->grupo->push($this->input->input_stream()); 
          break;
        case 'patch':
          $this->load->model('grupo');
          return $this->grupo->edit($this->input->input_stream()); 
          break;
      }

      $this->data['title'] = 'Grupos'; 
      $this->data['paper'] = 'table';
      $this->data['resrc'] = 'grupos';

      $this->load->view('platform', $this->data);
    }

    public function editar() 
    {
      $this->load->model('grupo');

      if (is_numeric($this->uri->segment(3)))
      {
        if ($this->grupo->seek(['grupo_id' => $this->uri->segment(3)])) 
        {
          $data['title'] = 'Editar grupo';
          $data['metod'] = 'patch';
        }
        else
        {
          show_error('La solicitud fue legal, pero el sistema rehúsa responderla dado que usted no tiene los privilegios para hacerla.', 403, 'Permiso denegado');
        }
      }
      else
      {
        $data['title'] = 'Agregar grupo';
        $data['metod'] = 'post';
      }

      $data['paper'] = 'edit/grupos';
      $data['resrc'] = 'grupos?grupo_id='.$this->uri->segment(3);

      $this->load->view('platform', $data);
    }
    
    public function json() 
    {
      $this->load->model('grupo');

      echo json_encode($this->grupo->seek($this->input->get()));
    }

    public function reporte()
    {
      $alumno_id = $this->uri->segment(3);

      if ($alumno_id) {
        $data['alumno'] = $this->db->query("select concat(alumno_rut,'-',alumno_verificador) as rut, concat(alumno_nombres,' ', coalesce(alumno_apellidop, ''), '', coalesce(alumno_apellidom, '')) as nombre ,concat(curso_grado, ' ', curso_letra) as curso, concat(usuario_nombre,' ',usuario_appat) as docente, Year(curso_periodo) as anio
              from alumno as al
              inner join curso_alumno as cural on al.alumno_id = cural.alumno_id
              inner join curso as cur on cur.curso_id = cural.curso_id
              inner join usuario as us on us.usuario_id = cur.usuario_id
              where al.alumno_id = $alumno_id and Year(curso_periodo) = Year(curdate())");

        $data['resultados'] = $this->db->query("Select  actividad_nombre, actividad_id, alumno_id, aplicacion_nombre, actividad_numero,
              round((resultado*100)/total) as percent, (case when ((resultado*100)/total) < 10 then 'No logrado' when ((resultado*100)/total) <25 then 'Levemente logrado' when ((resultado*100)/total) <50 then 'Medianamente logrado' else 'Logrado' end) as nivel
              from(
              Select act.actividad_nombre, act.actividad_id, alumno_id, aplicacion_nombre, actividad_numero, Sum(case when resultado_oportunidad = 1 then 3
              when resultado_oportunidad = 2 then 2 when resultado_oportunidad = 3 then (
              case when resultado_correcta = 0 then 0 else 1 end) end) as resultado, total
              from aplicacion as app
              inner join actividad as act on app.aplicacion_id = act.aplicacion_id
              inner join ejercicio as eje on eje.actividad_id = act.actividad_id
              inner join resultado as res on res.ejercicio_id = eje.ejercicio_id
              inner join (select actividad_id, ((count(ejercicio_id))*3) as total from ejercicio group by actividad_id) as pjeTotal on pjeTotal.actividad_id = act.actividad_id
              group by alumno_id, aplicacion_nombre, actividad_numero, total
              ) as x
              where alumno_id = $alumno_id
              order by aplicacion_nombre, actividad_numero");
      }

      $this->load->view('report', $data);
    }
    
    public function docentes()
    {
      switch($this->input->method()) 
      {
        case 'post':
          return $this->db->insert('docente_grupo', $this->input->input_stream()); 
          break;
        case 'delete':
          return $this->db->delete('docente_grupo', $this->input->input_stream()); 
          break;
      }
      
      switch ($this->uri->segment(3))
      {
        case 'json':
          if (isset($_GET['_'])) { unset($_GET['_']); }
          
          $this->db->select('*');
          $this->db->from('docente_grupo');
          
          $this->db->where($this->input->get());
          
          echo json_encode($this->db->get()->result());
          break;
      }
    }
  }
