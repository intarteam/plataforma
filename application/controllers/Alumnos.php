<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Alumnos extends CI_Controller {

    function __construct() 
    {
      parent::__construct();
      if (!$this->session->userdata('usuario_id')) { redirect('ingresar', 'refresh'); }
    }

    public function index()
    {
      switch($this->input->method()) 
      {
        case 'post':
          $this->load->model('alumno');
          return $this->alumno->push($this->input->input_stream()); 
          break;
        case 'patch':
          $this->load->model('alumno');
          return $this->alumno->edit($this->input->input_stream()); 
          break;
      }

      $this->data['title'] = 'Gestión de alumnos';
      $this->data['paper'] = 'table';
      $this->data['resrc'] = 'alumnos';

      $this->load->view('platform', $this->data);
    }

    public function editar()
    {
      $this->load->model('alumno');

      if (is_numeric($this->uri->segment(3)))
      {
        if ($this->alumno->seek(['alumno.alumno_id' => $this->uri->segment(3)])) 
        {
          $data['title'] = 'Editar alumno';
          $data['metod'] = 'patch';
        }
        else
        {
          show_error('La solicitud fue legal, pero el sistema reh�sa responderla dado que usted no tiene los privilegios para hacerla.', 403, 'Permiso denegado');
        }
      }
      else
      {        
        $data['title'] = 'Agregar alumno';
        $data['metod'] = 'post';
      }

      $data['paper'] = 'edit/alumnos';
      $data['resrc'] = 'alumnos?alumno_id='.$this->uri->segment(3);

      $this->load->view('platform', $data);
    }
    
    public function json() 
    {
      $this->load->model('alumno');

      echo json_encode($this->alumno->seek($this->input->get()));
    }

        public function reporte()
        {
            $alumno_id = $this->uri->segment(3);

            if ($alumno_id) {
                $data['alumno'] = $this->db->query("
select
    concat(alumno_rut,'-',alumno_verificador) as rut,
    concat(alumno_nombres,' ', coalesce(alumno_apellidop, ''), ' ', coalesce(alumno_apellidom, '')) as nombre,
    concat(curso_grado, '', curso_letra) as curso,
    concat(usuario_nombre,' ',usuario_appat) as docente,
    year(curso_periodo) as anio
from
    alumno as al
    inner join curso_alumno as cural on al.alumno_id = cural.alumno_id
    inner join curso as cur on cur.curso_id = cural.curso_id
    inner join usuario as us on us.usuario_id = cur.usuario_id
where 
    al.alumno_id = $alumno_id
order by 
    curso_periodo desc limit 1");

                $periodo = $data['alumno']->result()[0]->anio;

                $data['resultados'] = $this->db->query("
select
    actividad_nombre,
    actividad_id,
    alumno_id,
    aplicacion_nombre,
    actividad_numero,
    resultado,
    total,
    round((resultado*100)/total) as percent,
    (case when ((resultado*100)/total) is null then 'No logrado' when ((resultado*100)/total) < 10 then 'No logrado' when ((resultado*100)/total) <25 then 'Levemente logrado' when ((resultado*100)/total) <50 then 'Medianamente logrado' else 'Logrado' end) as nivel
from (
    select
        act.actividad_nombre,
        act.actividad_id,
        alumno_id,
        aplicacion_nombre,
        actividad_numero,
        sum(case 
                when resultado_oportunidad = 1 then 3 
                when resultado_oportunidad = 2 then 2 
                when resultado_oportunidad = 3 then (
                case 
                    when resultado_correcta = 0 then 0 else 1 
                end)
                when resultado_oportunidad > 3 then 0
            end) as resultado,
        count(*) * 3 as total
    from 
        aplicacion as app
        inner join actividad as act on app.aplicacion_id = act.aplicacion_id
        inner join ejercicio as eje on eje.actividad_id = act.actividad_id
        inner join resultado as res on res.ejercicio_id = eje.ejercicio_id and year(res.resultado_fechahora) = ".$periodo."
        inner join (
            select 
                actividad_id,
                count(ejercicio_id) as total
            from 
                ejercicio
            group by 
                actividad_id
            ) as ejercicio on ejercicio.actividad_id = act.actividad_id
        group by 
            actividad_nombre,
            actividad_id,
            alumno_id,
            aplicacion_nombre,
            actividad_numero,
            total
        ) as x
    where 
        alumno_id = $alumno_id
    order by
        aplicacion_nombre,
        actividad_numero");
      }

      $this->load->view('report', $data);
    }
    
    public function diagnosticos()
    {
      switch($this->input->method()) 
      {
        case 'post':
          return $this->db->insert('alumno_diagnostico', $this->input->input_stream()); 
          break;
        case 'delete':
          return $this->db->delete('alumno_diagnostico', $this->input->input_stream()); 
          break;
      }
      
      switch ($this->uri->segment(3))
      {
        case 'json':
          if (isset($_GET['_'])) { unset($_GET['_']); }
          
          $this->db->select('*');
          $this->db->from('diagnostico');
          $this->db->join('tipo_diagnostico', 'diagnostico.tipod_id = tipo_diagnostico.tipod_id');
          
          if ($this->input->get('alumno_id')) 
          { 
            $_GET['alumno_diagnostico.alumno_id'] = $_GET['alumno_id']; unset($_GET['alumno_id']);
            
            $this->db->join('alumno_diagnostico', 'diagnostico.diagnostico_id = alumno_diagnostico.diagnostico_id');
          }
          
          $this->db->where($this->input->get());
          
          echo json_encode($this->db->get()->result());
          break;
      }
    }
    
    public function importar()
    {
      if ($this->input->method() == 'post')
      {
        $this->load->model('alumno');
        $data = json_decode(file_get_contents('php://input'), true);
        
        $errores = array();
        
        $comuna_id = $this->db->query('select * from institucion where institucion_id = '.$this->input->get('institucion_id'))->result()[0]->comuna_id;
        
        if (!$comuna_id) {
          $errores []= ['title'=>'Ouch!', 'message'=>'Institucion sin comuna.'];
          
        }
        
        foreach($data as $alumno)
        {
          $alumno['comuna_id'] = $comuna_id;
          $alumno['institucion_id'] = $this->input->get('institucion_id');
          $alumno['alumno_nacimiento'] = strtr($alumno['alumno_nacimiento'], '/', '-');
          $alumno['alumno_nacimiento'] = date('Y-m-d', strtotime($alumno['alumno_nacimiento']));
          
          if (!$this->alumno->push($alumno))
          {
            $errores []= ['title'=>'Ouch!', 'message'=>'El alumno '.$alumno['alumno_rut'].' ya esta registrado.'];
          }
          
        }
        
        ob_end_clean();
        
        if ($errores)
        {
          http_response_code(400);
          echo json_encode($errores);
        }
        else
        {
          http_response_code(200); 
          echo json_encode(['title'=>'Yeah!', 'message'=>"Los alumnos fueron agregados."]); 
        }
      }
    }
  }
