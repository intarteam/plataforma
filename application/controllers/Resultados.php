<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resultados extends CI_Controller {

  function __construct() 
  {
    parent::__construct();
    if (!$this->session->userdata('usuario_id')) 
    { 
      redirect('ingresar', 'refresh'); 
    }
    
    if (isset($_GET['_'])) 
    {
      unset($_GET['_']);
    }
  }
  
  public function index()
  {

    if (!$_GET) { show_error('La solicitud contiene sintaxis erronea y no deberia repetirse.', 400, 'Solicitud incorrecta'); }
    
    $data['title'] = 'Resultados';
    $data['paper'] = 'chart';
    
    $this->load->view('platform', $data);
  }
  
  public function json() 
  {
    if(isset($_GET['aplicacion_id']))
    {
      $_GET['aplicacion.aplicacion_id'] = $_GET['aplicacion_id'];
      unset($_GET['aplicacion_id']);
    }
    
    if(isset($_GET['actividad_id']))
    {
      $_GET['actividad.actividad_id'] = $_GET['actividad_id'];
      unset($_GET['actividad_id']);
    }
    
    if(isset($_GET['ejercicio_id']))
    {
      $_GET['ejercicio.ejercicio_id'] = $_GET['ejercicio_id'];
      unset($_GET['ejercicio_id']);
    }
    
    if(isset($_GET['alumno_id']))
    {
      $_GET['alumno.alumno_id'] = $_GET['alumno_id'];
      unset($_GET['alumno_id']);
    }
    
    if(isset($_GET['curso_id']))
    {
      $_GET['curso.curso_id'] = $_GET['curso_id'];
      unset($_GET['curso_id']);
    }
    
    if(isset($_GET['grupo_id']))
    {
      $_GET['grupo.grupo_id'] = $_GET['grupo_id'];
      unset($_GET['grupo_id']);
    }
    
    if(isset($_GET['usuario_id']))
    {
      $_GET['usuario.usuario_id'] = $_GET['usuario_id'];
      unset($_GET['usuario_id']);
    }
    
    if(isset($_GET['institucion_id']))
    {
      $_GET['institucion.institucion_id'] = $_GET['institucion_id'];
      unset($_GET['institucion_id']);
    }
    
    //$_GET['Year(fecha_incorporacion)'] = date("Y");
    
    if ($this->session->userdata('tipo_id') > 1) {
      if(isset($_GET['institucion.institucion_id']) and $_GET['institucion.institucion_id'] != $this->session->userdata('institucion_id')) {
        echo json_encode([]);
        return;
      }
      
      $_GET['institucion.institucion_id'] = $this->session->userdata('institucion_id');
    }
    
    $this->db->select("*, DATE_FORMAT(resultado.resultado_fechahora,'%d/%m/%Y') as resultado_fechahora");
    $this->db->from('resultado');
    $this->db->join('ejercicio', 'resultado.ejercicio_id = ejercicio.ejercicio_id');
    $this->db->join('actividad', 'ejercicio.actividad_id = actividad.actividad_id');
    $this->db->join('aplicacion', 'actividad.aplicacion_id = aplicacion.aplicacion_id');
    $this->db->join('alumno', 'resultado.alumno_id = alumno.alumno_id');
    $this->db->join('curso_alumno', 'alumno.alumno_id = curso_alumno.alumno_id');
    $this->db->join('curso', 'curso_alumno.curso_id = curso.curso_id');
    $this->db->join('usuario', 'curso.usuario_id = usuario.usuario_id');
    $this->db->join('institucion', 'usuario.institucion_id = institucion.institucion_id');
    $this->db->join('grupo', 'alumno.grupo_id = grupo.grupo_id', 'left');
    $this->db->where($this->input->get());
    $this->db->where('year(curso_alumno.fecha_incorporacion) = year(resultado.resultado_fechahora)');
    $this->db->order_by("resultado.resultado_fechahora", "ascc");
    
    echo json_encode($this->db->get()->result());
  }
}
