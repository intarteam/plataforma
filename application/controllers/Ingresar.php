<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class Ingresar extends CI_Controller 
  {
    function __construct() 
    {
      parent::__construct();
      if ($this->session->userdata('usuario_id')) { redirect('/', 'refresh'); } 
    }

    public function index()
    {	    
      if ($this->input->post()) 
      {
        $this->load->model('usuario');
        
        $usuario = $this->usuario->seek(['usuario_rut'=>$_POST['usuario_rut'], 'usuario_clave'=>md5($_POST['usuario_clave'])]);
        
        if ($usuario) 
        {
          $usuario = $usuario[0];
          
          $this->session->set_userdata('usuario_id', $usuario->usuario_id);
          $this->session->set_userdata('usuario_nombre', $usuario->usuario_nombre.' '.$usuario->usuario_appat);
          $this->session->set_userdata('usuario_imagen', $usuario->usuario_imagen_url);
          $this->session->set_userdata('tipo_id', $usuario->tipo_id);
          $this->session->set_userdata('institucion_imagen', $usuario->institucion_imagen);
          $this->session->set_userdata('institucion_id', $usuario->institucion_id);

          return $this->output->set_status_header(200);
        }
      }

      $this->load->view('login');
    }
  }
