<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class Bienvenido extends CI_Controller {

    function __construct() 
    {
      parent::__construct();
      if (!$this->session->userdata('usuario_id')) { redirect('ingresar', 'refresh'); }
    }

    public function index()
    {
      $data['title'] = 'Bienvenido';
      $data['paper'] = 'welcome';
      
      $this->load->view('platform', $data);
    }
  }
