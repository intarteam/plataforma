<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  class Cursos extends CI_Controller {

    function __construct() 
    {
      parent::__construct();
      if (!$this->session->userdata('usuario_id')) { redirect('ingresar', 'refresh'); }
    }

    public function index()
    {
      switch($this->input->method()) 
      {
        case 'post':
          $this->load->model('curso');
          return $this->curso->push($this->input->input_stream()); 
          break;
        case 'patch':
          $this->load->model('curso');
          return $this->curso->edit($this->input->input_stream()); 
          break;
      }

      $this->data['title'] = 'Cursos'; 
      $this->data['paper'] = 'table';
      $this->data['resrc'] = 'cursos';

      $this->load->view('platform', $this->data);
    }

    public function editar() 
    {
      $this->load->model('curso');

      if (is_numeric($this->uri->segment(3)))
      {
        if ($this->curso->seek(['curso_id' => $this->uri->segment(3)])) 
        {
          $data['title'] = 'Editar curso';
          $data['metod'] = 'patch';
        }
        else
        {
          show_error('La solicitud fue legal, pero el sistema rehúsa responderla dado que usted no tiene los privilegios para hacerla.', 403, 'Permiso denegado');
        }
      }
      else
      {
        $data['title'] = 'Agregar curso';
        $data['metod'] = 'post';
      }

      $data['paper'] = 'edit/cursos';
      $data['resrc'] = 'cursos?curso_id='.$this->uri->segment(3);

      $this->load->view('platform', $data);
    }
    
    public function json() 
    {
      $this->load->model('curso');

      echo json_encode($this->curso->seek($this->input->get()));
    }

    public function reporte()
    {
      $alumno_id = $this->uri->segment(3);

      if ($alumno_id) {
        $data['alumno'] = $this->db->query("select concat(alumno_rut,'-',alumno_verificador) as rut, concat(alumno_nombres,' ', coalesce(alumno_apellidop, ''), '', coalesce(alumno_apellidom, '')) as nombre ,concat(curso_grado, ' ', curso_letra) as curso, concat(usuario_nombre,' ',usuario_appat) as docente, Year(curso_periodo) as anio
              from alumno as al
              inner join curso_alumno as cural on al.alumno_id = cural.alumno_id
              inner join curso as cur on cur.curso_id = cural.curso_id
              inner join usuario as us on us.usuario_id = cur.usuario_id
              where al.alumno_id = $alumno_id and Year(curso_periodo) = Year(curdate())");

        $data['resultados'] = $this->db->query("Select  actividad_nombre, actividad_id, alumno_id, aplicacion_nombre, actividad_numero,
              round((resultado*100)/total) as percent, (case when ((resultado*100)/total) < 10 then 'No logrado' when ((resultado*100)/total) <25 then 'Levemente logrado' when ((resultado*100)/total) <50 then 'Medianamente logrado' else 'Logrado' end) as nivel
              from(
              Select act.actividad_nombre, act.actividad_id, alumno_id, aplicacion_nombre, actividad_numero, Sum(case when resultado_oportunidad = 1 then 3
              when resultado_oportunidad = 2 then 2 when resultado_oportunidad = 3 then (
              case when resultado_correcta = 0 then 0 else 1 end) end) as resultado, total
              from aplicacion as app
              inner join actividad as act on app.aplicacion_id = act.aplicacion_id
              inner join ejercicio as eje on eje.actividad_id = act.actividad_id
              inner join resultado as res on res.ejercicio_id = eje.ejercicio_id
              inner join (select actividad_id, ((count(ejercicio_id))*3) as total from ejercicio group by actividad_id) as pjeTotal on pjeTotal.actividad_id = act.actividad_id
              group by alumno_id, aplicacion_nombre, actividad_numero, total
              ) as x
              where alumno_id = $alumno_id
              order by aplicacion_nombre, actividad_numero");
      }

      $this->load->view('report', $data);
    }
    
    public function alumnos()
    {
      switch($this->input->method()) 
      {
        case 'post':
          $data = $this->input->input_stream();
          $data['fecha_incorporacion'] = date('Y-m-d');
          return $this->db->insert('curso_alumno', $data); 
          break;
        case 'patch':
          $data = $this->input->input_stream();
          if (isset($data['retiro_motivo'])) { $data['fecha_retiro'] = date('Y-m-d'); }
          return $this->db->update('curso_alumno', $data, $this->input->get()); 
          break;
        case 'delete':
          return $this->db->delete('curso_alumno', $this->input->get()); 
          break;
      }
      
      switch ($this->uri->segment(3))
      {
        case 'json':
          if (isset($_GET['_'])) { unset($_GET['_']); }
          
          if (isset($_GET['alumno_id'])) { $_GET['alumno.alumno_id'] = $_GET['alumno_id']; unset($_GET['alumno_id']); }
          
          $this->db->select('*');
          $this->db->from('alumno');
          $this->db->join('curso_alumno', 'alumno.alumno_id = curso_alumno.alumno_id');
          
          if ($this->input->get('curso_id')) 
          { 
            $_GET['curso.curso_id'] = $_GET['curso_id']; unset($_GET['curso_id']);
            
            $this->db->join('curso', 'curso.curso_id = curso_alumno.curso_id');
          }
          
          $this->db->where($this->input->get());
          
          echo json_encode($this->db->get()->result());
          break;
      }
    }
  }
