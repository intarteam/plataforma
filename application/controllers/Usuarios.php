<?php 
  
  defined('BASEPATH') OR exit('No direct script access allowed');

  class Usuarios extends CI_Controller {

    function __construct() 
    {
      parent::__construct();
      if (!$this->session->userdata('usuario_id')) { redirect('ingresar', 'refresh'); }
    }

    public function index()
    {     
      switch($this->input->method()) 
      {
        case 'post':
          $this->load->model('usuario');
          return $this->usuario->push($this->input->input_stream()); 
          break;
        case 'patch':
          $this->load->model('usuario');
          return $this->usuario->edit($this->input->input_stream()); 
          break;
      }

      switch($this->session->userdata('tipo_id'))
      {
        case 1: 
          $data['title'] = 'Gestión de usuarios'; 
          $data['paper'] = 'table';
          $data['resrc'] = 'usuarios';
          break;
        case 2: 
          $data['title'] = 'Gestión de docentes'; 
          $data['paper'] = 'table';
          $data['resrc'] = 'usuarios?tipo_id=3';
          break;
        case 3: 
          return show_error('La solicitud fue correcta, pero el sistema rehúsa responderla dado que usted no tiene los privilegios para hacerla.', 403, 'Permiso denegado');
          break;
      }

      $this->load->view('platform', $data);
    }

    public function editar() 
    {
      $this->load->model('usuario');
      
      $data['title'] = 'usuarios';
      if ($this->session->userdata('tipo_id') > 1) {
        $data['title'] = 'docentes';
      }

      if (is_numeric($this->uri->segment(3)))
      {
        if ($this->usuario->seek(['usuario_id' => $this->uri->segment(3)])) 
        {
          $data['title'] = 'Editar ' .$data['title'];
          $data['metod'] = 'patch';
        }
        else
        {
          show_error('La solicitud fue legal, pero el sistema rehúsa responderla dado que usted no tiene los privilegios para hacerla.', 403, 'Permiso denegado');
        }
      }
      else
      {
        if ($this->session->userdata('tipo_id') < 3) 
        {
          $data['title'] = 'Agregar '.$data['title'];
          $data['metod'] = 'post';
        }
        else
        {
          show_error('La solicitud fue legal, pero el sistema rehúsa responderla dado que usted no tiene los privilegios para hacerla.', 403, 'Permiso denegado');
        }
      }

      if ($this->uri->segment(3) == $this->session->userdata('usuario_id')) { $data['title'] = 'Editar mis datos'; }

      $data['paper'] = 'edit/usuarios';
      $data['resrc'] = 'usuarios?usuario_id='.$this->uri->segment(3);

      $this->load->view('platform', $data);
    }

    public function json() 
    {
      $this->load->model('usuario');

      echo json_encode($this->usuario->seek($this->input->get()));
    }

    public function logout()
    {
      $this->session->unset_userdata('usuario_id');
      $this->session->unset_userdata('usuario_nombre');
      $this->session->unset_userdata('tipo_id');
      $this->session->unset_userdata('usuario_imagen');
      $this->session->unset_userdata('institucion_id');
      $this->session->unset_userdata('institucion_imagen');

      redirect('/ingresar/', 'refresh');
    }

    public function imagen()
    {
      if (isset($_FILES['usuario_imagen']) and isset($_GET['usuario_id']))
      {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $img = $_FILES['usuario_imagen'];

        $filename = $img['tmp_name'];
        $client_id="ad929d15778b33c";
        $handle = fopen($filename, "r");
        $data = fread($handle, filesize($filename));
        $pvars = array('image' => base64_encode($data));
        $timeout = 30;

        curl_setopt($curl, CURLOPT_URL, 'https://api.imgur.com/3/image.json');
        curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Client-ID ' . $client_id));
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $pvars);

        $out = curl_exec($curl);
        curl_close ($curl);

        $pms = json_decode($out,true);
        $url = $pms['data']['link'];

        if($url != "") {
          $this->db->update('usuario', ['usuario_imagen_url'=>$url], "usuario_id = ".$this->input->get('usuario_id'));
          if ($this->input->get('usuario_id') == $this->session->userdata('usuario_id'))
          {
            $this->session->set_userdata('usuario_imagen', $url);
          }
        } 
      }
    }
  }
