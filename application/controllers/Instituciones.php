<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class Instituciones extends CI_Controller {
    
    function __construct() 
    {
      parent::__construct();
      if (!$this->session->userdata('usuario_id')) { redirect('ingresar', 'refresh'); }
    }

    public function index()
    {    
      switch($this->input->method()) 
      {
        case 'post':
          $this->load->model('institucion');
          return $this->institucion->push($this->input->input_stream()); 
          break;
        case 'patch':
          $this->load->model('institucion');
          return $this->institucion->edit($this->input->input_stream()); 
          break;
      }

      if ($this->session->userdata('tipo_id') == 1)
      {
        $data['title'] = 'Instituciones'; 
        $data['paper'] = 'table';
        $data['resrc'] = 'instituciones';
      }
      else
      {
        return show_error('La solicitud fue legal, pero el sistema rehúsa responderla dado que usted no tiene los privilegios para hacerla.', 403, 'Permiso denegado');
      }

      $this->load->view('platform', $data);
    }

    public function editar() 
    {
      $this->load->model('institucion');

      if (is_numeric($this->uri->segment(3)))
      {
        if ($this->institucion->seek(['institucion_id' => $this->uri->segment(3)])) 
        {
          $data['title'] = 'Editar institución';
          $data['metod'] = 'patch';
          
          if ($this->uri->segment(3) == $this->session->userdata('institucion_id')){
            $data['title'] = 'Editar mi institución';
          }

          if ($this->session->userdata('tipo_id') > 2) {
            $data['title'] = 'Ver datos de mi institución';
          }
        }
        else
        {
          show_error('La solicitud fue legal, pero el sistema rehúsa responderla dado que usted no tiene los privilegios para hacerla.', 403, 'Permiso denegado');
        }
      }
      else
      {
        if ($this->session->userdata('tipo_id') < 2) 
        {
          $data['title'] = 'Agregar institución';
          $data['metod'] = 'post';
        }
        else
        {
          show_error('La solicitud fue legal, pero el sistema rehúsa responderla dado que usted no tiene los privilegios para hacerla.', 403, 'Permiso denegado');
        }
      }

      if ($this->uri->segment(3) == $this->session->userdata('usuario_id')) { $data['title'] = 'Editar mi institución'; }

      $data['paper'] = 'edit/instituciones';
      $data['resrc'] = 'instituciones?institucion_id='.$this->uri->segment(3);

      $this->load->view('platform', $data);
    }
    
    public function json() {
      $this->load->model('institucion');

      echo json_encode($this->institucion->seek($this->input->get()));
    }
    
    public function imagen()
    {
      if (isset($_FILES['institucion_imagen']) and isset($_GET['institucion_id']))
      {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $img = $_FILES['institucion_imagen'];

        $filename = $img['tmp_name'];
        $client_id="ad929d15778b33c";
        if ( gethostname() != 'diego-pc') {
          $client_id="d181596e54c0658";
        }
        $handle = fopen($filename, "r");
        $data = fread($handle, filesize($filename));
        $pvars = array('image' => base64_encode($data));
        $timeout = 30;

        curl_setopt($curl, CURLOPT_URL, 'https://api.imgur.com/3/image.json');
        curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Client-ID ' . $client_id));
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $pvars);

        $out = curl_exec($curl);
        curl_close ($curl);

        $pms = json_decode($out,true);
        $url = $pms['data']['link'];

        if($url != "") {
          $this->db->update('institucion', ['institucion_imagen'=>$url], "institucion_id = ".$this->input->get('institucion_id'));
          if ($this->input->get('institucion_id') == $this->session->userdata('institucion_id'))
          {
            $this->session->set_userdata('institucion_imagen', $url);
          }
        } 
      }
    }
  }
