<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datatable {
  
  public function alumnos(&$data)
  {
    $data['datatable']['icon'] = 'child';
    $data['datatable']['item'] = 'alumno';
    $data['datatable']['title'] = 'Alumnos';
    $data['datatable']['source'] = 'alumnos';
    
    $data['datatable']['columns'] = [
      ['title' => 'ID', 'data' => 'alumno_id', 'visible' => false],
      ['title' => '# RUT', 'data' => 'alumno_rut'],
      ['title' => 'Nombres', 'data' => 'alumno_nombres'],
      ['title' => 'Apellidos', 'data' => 'alumno_apellidos'],
      ['title' => 'Institucion', 'data' => 'institucion_nombre'],
      ['title' => 'Comuna', 'data' => 'comuna_nombre']
    ];
    
    $data['datatable']['filter'] = [4, 5];
    
    $data['form']['source'] = 'alumnos';
    $data['form']['method'] = 'patch';
    $data['form']['uri'] = 'alumnos';
    $data['form']['message'] = 'Alumno actualizado con exito';
    $data['form']['modal_header'] = '<h4 class="modal-title" id="myModalLabel">Descripcion del alumno</h4>';
    $data['form']['modal_footer'] = '<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button><button type="submit" class="btn btn-primary">Guardar</button>';
  }
  
  public function cursos(&$data)
  {
    $data['datatable']['icon'] = 'child';
    $data['datatable']['item'] = 'curso';
    $data['datatable']['title'] = 'Cursos';
    $data['datatable']['source'] = 'cursos';
    
    $data['datatable']['columns'] = [
      ['title' => 'ID', 'data' => 'curso_id', 'visible' => false],
      ['title' => '# Curso', 'data' => 'curso_grado_letra'],
      ['title' => 'Docente', 'data' => 'usuario_nombre'],
      ['title' => 'Periodo', 'data' => 'curso_periodo']
    ];
    
    $data['datatable']['filter'] = [2, 3];
    
    $data['form']['source'] = 'cursos';
    $data['form']['method'] = 'patch';
    $data['form']['uri'] = 'cursos';
    $data['form']['message'] = 'Curso actualizado con exito';
    $data['form']['modal_header'] = '<h4 class="modal-title" id="myModalLabel">Descripcion del curso</h4>';
    $data['form']['modal_footer'] = '<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button><button type="submit" class="btn btn-primary">Guardar</button>';
  }
  
  public function actividades(&$data)
  {
    $data['datatable']['icon'] = 'child';
    $data['datatable']['item'] = 'actividad';
    $data['datatable']['title'] = 'Actividad';
    $data['datatable']['source'] = 'actividades';
    
    $data['datatable']['columns'] = [
      ['title' => 'ID', 'data' => 'actividad_id', 'visible' => false],
      ['title' => '# Numero', 'data' => 'actividad_numero'],
      ['title' => 'Nombre', 'data' => 'actividad_nombre'],
      ['title' => 'Aplicacion', 'data' => 'aplicacion_nombre']
    ];
    
    $data['datatable']['filter'] = [3];
    
    $data['form']['source'] = 'actividades';
    $data['form']['method'] = 'patch';
    $data['form']['uri'] = 'actividades';
    $data['form']['message'] = 'Actividad actualizada con exito';
    $data['form']['modal_header'] = '<h4 class="modal-title" id="myModalLabel">Descripcion del curso</h4>';
    $data['form']['modal_footer'] = '<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button><button type="submit" class="btn btn-primary">Guardar</button>';
  }
}