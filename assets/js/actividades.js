var params = $.grep(
  window.location.pathname.substring(window.location.pathname.indexOf('actividades'), window.location.pathname.length).split('/'), 
  function(value) { 
    return value != '' 
  }
)

$(document).ready(function() {
  if (params[0] == 'actividades' && params.length == 1) {
    refreshTable('actividades/json')    
  }
})
                  
function refreshTable(src) {
  $.getJSON(src, function(data) {
    renderTable(data)
  })
}

function renderTable(data) {
  var table = $('#actividades').DataTable({
    data: data,
    columns: [
      { title: "ID", data: "actividad_id" },
      { title: "Actividad", data: "actividad_texto" },
      { title: "Objetivo", data: "actividad_nombre" },
      { title: "Aplicacion", data: "aplicacion_nombre" },
      {
        "className":      'details-control',
        "orderable":      false,
        "data":           null,
        "defaultContent": defContent()
      }
    ],
    initComplete: function() { 
      redesignTable(); 
      insertSelect(this.api()); 
      $(".panel").removeClass("loading") 
    },
    language: {
      lengthMenu: "Mostrar: _MENU_",
      info: "Mostrando _START_ hasta _END_ de _TOTAL_ actividades",
      search: "Filtrar: _INPUT_",
      infoFiltered: "(filtrado de _MAX_ actividades)",
      paginate: { previous: "Anterior", next: "Siguiente" }
    }
  })
  table.column( 3 ).visible( false )
  $('#actividades tbody').on( 'click', 'button', function () {
      refreshModal( table.row( $(this).parents('tr') ).data().actividad_id );
  })
}

function insertSelect(table) {
  var column = table.column( 3 )
  var select = $(defSelect())
  .appendTo("#actividades_length" )
  .children('select')
  .css("width", "auto")
  .on('change', function() {
    var val = $.fn.dataTable.util.escapeRegex($(this).val())
    column
      .search( val ? '^'+val+'$' : '', true, false )
      .draw()
  })
  column.data().unique().sort().each( function ( d, j ) {
    select.append('<option value="'+d+'">'+d+'</option>')
  })
}

function redesignTable() {
  $('#actividades_filter, #actividades_paginate')
    .css("margin-right", "15px")
  
  $('#actividades_length, #actividades_info')
    .css("margin-left", "15px")
  
  $("#actividades")
    .css("width", "100%")
    .css("border-bottom", "1px solid #ddd")
    .css("border-top", "1px solid #ddd")
    .css('font-size', '14px')
  
  $('#actividades_wrapper')
    .css('padding-top', '10px')
    .css('padding-bottom', '10px')
    .css('font-size', '14px')
}

function defContent() {
  return '<div class="btn-group btn-group-xs" role="group" aria-label="...">' + 
    '<button data-toggle="modal" data-target="#actividad" type="button" class="btn btn-default"><i class="fa fa-info"></i></button> ' +
    '<a type="button" class="btn btn-default"><i class="fa fa-area-chart"></i></a></div>';
}

function defSelect() {
  return '<label>&nbsp;&nbsp;Aplicacion: ' +
    '<select aria-controls="actividades" class="form-control input-sm"><option value="">Todos</option></select></label>';
}

function refreshModal(id) {
  $.getJSON("actividades/" + id + "/json", function(data) {
    renderModal(data)
  })
}

function renderModal(data) {
  $(".modal input, .modal select").val('')
  
  $('.modal input[name="id"]').val(data.actividad_id)
  $('.modal input[name="num"]').val(data.actividad_numero)
  $('.modal input[name="nom"]').val(data.actividad_nombre)
  $('.modal textarea[name="des"]').val(data.actividad_descripcion)
  $('.modal input[name="obj"]').val(data.actividad_objetivo)
  $('.modal input[name="tex"]').val(data.actividad_texto)
  $('.modal select[name="app"]').val(data.aplicacion_id)
  
}