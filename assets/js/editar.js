$(document).ready(function() {
    /**
     *
     * @type {*}
     */
    var $form = $('form');

    /**
     *
     */
    $.getJSON( '/plataforma/index.php/' + $form.attr('source').split('?')[0] + '/json?' + $form.attr('source').split('?')[1], fillForm);

    /**
     *
     * @param data
     */
    function fillForm(data) {
        if ($form.attr('source').split('=')[1] != '' && data.length < 1) {
            $('form').remove();
            $('h1').text('Error');
            $('.col-lg-12').append('<p>Acceso denegado.</p>')
        } else {
            data.forEach(fillFormControl)
        }

        $('.progress').fadeOut()
    }

    /**
     *
     * @param obj
     */
    function fillFormControl(obj) {
        for (var property in obj) {
            var $input = $('[name="' + property + '"][type!="file"][type!="password"]');

            if ($input.length > 0 && obj.hasOwnProperty(property)) {
                switch ($input.prop('tagName').toLowerCase()) {
                    case 'input': $input.val(obj[property]);
                        break;
                    case 'select': $input.children('option[value="' + obj[property] + '"]').attr('selected', true);
                        break;
                }
            }
        }
    }

    $form.on('submit', function(e){
    e.preventDefault()

    $('.progress').fadeIn()
    
    if ($('form input[type="file"]').length > 0)
    {
      var file = $('input[type="file"]')[0].files[0], dataImagen = new FormData() 

      dataImagen.append($('input[type="file"]').attr('name'), file)

      $.ajax({
        type:'post',
        url:"/plataforma/index.php/" + $('form').attr('source').split('?')[0] + "/imagen?" + $('form').attr('source').split('?')[1],
        contentType:false,
        data: dataImagen,
        processData:false,
        cache:false
      })
    }

    $.ajax({ 
      type: $('form').attr('method'),
      url: '/plataforma/index.php/' + $('form').attr('source'), 
      data: $('form').serialize(),
      dataType: 'json'
    })
    .always(function() {
      $('.progress').fadeOut()  
    })
    .done(function(data) {
      $.growl.notice(data)
    })
    .fail(function(data) {
      $.growl.error(JSON.parse(data.responseText))
    })

    $('form').validator('validate')
  })
})