var params = $.grep(
  window.location.pathname.substring(window.location.pathname.indexOf('cursos'), window.location.pathname.length).split('/'), 
  function(value) { 
    return value != '' 
  }
)

$(document).ready(function() {
  if (params[0] == 'cursos' && params.length == 1) {
    refreshTable('cursos/json')    
  }
})
                  
function refreshTable(src) {
  $.getJSON(src, function(data) {
    renderTable(data)
  })
}

function renderTable(data) {
  var table = $('#cursos').DataTable({
    data: data,
    columns: [
      { title: "ID", data: "curso_id" },
      { title: "Grado", data: "grado_letra" },
      { title: "Institucion", data: "institucion_nombre" },
      { title: "Docente", data: "docente_nombre" },
      { title: "Periodo", data: "curso_periodo" },
      {
        "className":      'details-control',
        "orderable":      false,
        "data":           null,
        "defaultContent": defContent()
      }
    ],
    columnDefs: [ {
      targets: 1,
      render: $.fn.dataTable.render.ellipsis( 50 )
    },
                {
      targets: 2,
      render: $.fn.dataTable.render.ellipsis( 50 )
    }],
    initComplete: function() { 
      redesignTable(); 
      insertSelect(this.api()); 
      $(".panel").removeClass("loading") 
    },
    language: {
      lengthMenu: "<strong>Mostrar:</strong> _MENU_",
      info: "Mostrando _START_ hasta _END_ de _TOTAL_ cursos",
      search: "<strong>Filtrar:</strong> _INPUT_",
      infoFiltered: "(filtrado de _MAX_ actividades)",
      paginate: { previous: "Anterior", next: "Siguiente" }
    }
  })
  //table.column( 3 ).visible( false )
  $('#cursos tbody').on( 'click', 'button', function () {
      refreshModal( table.row( $(this).parents('tr') ).data().curso_id );
  })
  $('#cursos tbody').on( 'click', 'a', function () {
      location.replace('resultados?curso=' + table.row( $(this).parents('tr') ).data().curso_id )
  })
  $('.modal .btn-primary').click(function() {
    $(".modal input, .modal select").css("disabled", true)
    $(".alert").removeClass("alert-warning").removeClass("alert-success").hide()
    $.ajax({
      url: 'cursos/' + $('.modal input[name="id"]').val(),
      type: 'PUT',
      data: {
        id:  $('.modal input[name="id"]').val(),
        gra: $('.modal input[name="gra"]').val(),
        let: $('.modal input[name="let"]').val(),
        per: $('.modal input[name="per"]').val(),
        doc: $('.modal select[name="doc"]').val(),
        ins: $('.modal select[name="ins"]').val()
      },
      success: function(data) {
        location.reload()
      },
      always: function() {
        $(".modal input, .modal select").css("disabled", false)
      },
      error: function() {
        $(".alert").children("strong").text("Peligro!")
        $(".alert").children("text").text("Datos corruptos.")
        $(".alert").addClass("alert-danger").show()
      }
    })
  })
  $('.modal button.close').click(function () {
    $("div.alert").hide()
  })
}

function insertSelect(table) {
  var column = table.column( 4 )
  var select = $(defSelect())
  .appendTo("#cursos_length" )
  .children('select')
  .css("width", "auto")
  .on('change', function() {
    var val = $.fn.dataTable.util.escapeRegex($(this).val())
    column
      .search( val ? '^'+val+'$' : '', true, false )
      .draw()
  })
  column.data().unique().sort().each( function ( d, j ) {
    select.append('<option value="'+d+'">'+d+'</option>')
  })
}

function redesignTable() {
  $('#cursos_filter, #cursos_paginate')
    .css("margin-right", "15px")
  
  $('#cursos_length, #cursos_info')
    .css("margin-left", "15px")
  
  $("#cursos")
    .css("width", "100%")
    .css("border-bottom", "1px solid #ddd")
    .css("border-top", "1px solid #ddd")
    .css('font-size', 'medium')
  
  $('#cursos_wrapper')
    .css('padding-top', '10px')
    .css('padding-bottom', '10px')
    .css('font-size', '14px')
}

function defContent() {
  return '<div class="btn-group btn-group-xs" role="group" aria-label="...">' + 
    '<button data-toggle="modal" data-target="#curso" type="button" class="btn btn-default"><i class="fa fa-info"></i></button> ' +
    '<a type="button" class="btn btn-default"><i class="fa fa-area-chart"></i></a></div>';
}

function defSelect() {
  return '<label><strong>&nbsp;&nbsp;Periodo:</strong> ' +
    '<select aria-controls="periodos" class="form-control input-sm"><option value="">Todos</option></select></label>';
}

function refreshModal(id) {
  $.getJSON("cursos/" + id + "/json", function(data) {
    if (data) {
      renderModal(data[0])
    }
  })
}

function renderModal(data) {
  $.getJSON("docentes/json", function(docentes) {
    $('.modal select[name="ins"] option').attr('disabled', true)
    $('.modal select[name="doc"] option').remove()
    $.each(docentes, function(i, e) {
      $('.modal select[name="doc"]').append('<option value="' + e.usuario_id +'"> ' + e.usuario_nombre + ' ' + e.usuario_appat + '</option>')
    })
    $('.modal select[name="doc"]').val(data.usuario_id)
    $('.modal select[name="ins"] option').attr('disabled', false)
  })
  $.getJSON("institucion/json", function(institucion) {
    $('.modal select[name="ins"] option').attr('disabled', true)
    $('.modal select[name="ins"] option').remove()
    $.each(institucion, function(i, e) {
      $('.modal select[name="ins"]').append('<option value="' + e.institucion_id +'"> ' + e.institucion_nombre + '</option>')
    })
    $('.modal select[name="ins"]').val(data.institucion_id)
    $('.modal select[name="ins"] option').attr('disabled', false)
  })
  $(".modal input, .modal select").val('')
  $('.modal input[name="id"]').val(data.curso_id)
  $('.modal input[name="gra"]').val(data.curso_grado)
  $('.modal input[name="let"]').val(data.curso_letra)
  $('.modal input[name="per"]').val(data.curso_periodo)
}