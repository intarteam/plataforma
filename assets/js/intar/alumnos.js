var params = $.grep(
  window.location.pathname.substring(window.location.pathname.indexOf('alumnos'), window.location.pathname.length).split('/'), 
  function(value) { 
    return value != '' 
  }
)

$(document).ready(function() {
  if (params[0] == 'alumnos' && params.length == 1) {
    refreshTable('alumnos/json')    
  }
})
                  
function refreshTable(src) {
  $.getJSON(src, function(data) {
    renderTable(data)
  })
}

function renderTable(data) {
  var table = $('#alumnos').DataTable({
    data: data,
    columns: [
      { title: "ID", data: "alumno_id" },
      { title: "Apellido", data: "alumno_apellido" },
      { title: "Nombre", data: "alumno_nombres" },
      { title: "Curso", data: "curso_grado" },
      { title: "Institucion", data: "institucion_nombre" },
      {
        "className":      'details-control',
        "orderable":      false,
        "data":           null,
        "defaultContent": defContent()
      }
    ],
    columnDefs: [ {
      targets: 1,
      render: $.fn.dataTable.render.ellipsis( 50 )
    },
                {
      targets: 2,
      render: $.fn.dataTable.render.ellipsis( 50 )
    }],
    initComplete: function() { 
      redesignTable(); 
      insertSelect(this.api()); 
      $(".panel").removeClass("loading") 
    },
    language: {
      lengthMenu: "<strong>Mostrar:</strong> _MENU_",
      info: "Mostrando _START_ hasta _END_ de _TOTAL_ alumnos",
      search: "<strong>Filtrar:</strong> _INPUT_",
      infoFiltered: "(filtrado de _MAX_ alumnos)",
      paginate: { previous: "Anterior", next: "Siguiente" }
    }
  })
  //table.column( 3 ).visible( false )
  $('#alumnos tbody').on( 'click', 'button', function () {
      refreshModal( table.row( $(this).parents('tr') ).data().alumno_id );
  })
  $('#alumnos tbody').on( 'click', 'a', function () {
      location.replace('resultados?alumno=' + table.row( $(this).parents('tr') ).data().alumno_id )
  })
  $('.modal .btn-primary').click(function() {
    $(".modal input, .modal select, .modal textarea").css("disabled", true)
    $(".alert").removeClass("alert-warning").removeClass("alert-success").hide()
    $.ajax({
      url: 'alumnos/' + $('.modal input[name="id"]').val(),
      type: 'PUT',
      data: {
        id:  $('.modal input[name="id"]').val(),
        num: $('.modal input[name="num"]').val(),
        app: $('.modal select[name="app"]').val(),
        nom: $('.modal input[name="nom"]').val(),
        des: $('.modal textarea[name="des"]').val(),
        obj: $('.modal input[name="obj"]').val(),
        tex: $('.modal input[name="tex"]').val()
      },
      success: function(data) {
        location.reload()
      },
      always: function() {
        $(".modal input, .modal select, .modal textarea").css("disabled", false)
      },
      error: function() {
        $(".alert").children("strong").text("Peligro!")
        $(".alert").children("text").text("Datos corruptos.")
        $(".alert").addClass("alert-danger").show()
      }
    })
  })
  $('.modal button.close').click(function () {
    $("div.alert").hide()
  })
}

function insertSelect(table) {
  var column = table.column( 4 )
  var select = $(defSelect())
  .appendTo("#alumnos_length" )
  .children('select')
  .css("width", "auto")
  .on('change', function() {
    var val = $.fn.dataTable.util.escapeRegex($(this).val())
    column
      .search( val ? '^'+val+'$' : '', true, false )
      .draw()
  })
  column.data().unique().sort().each( function ( d, j ) {
    select.append('<option value="'+d+'">'+d+'</option>')
  })
}

function redesignTable() {
  $('#alumnos_filter, #alumnos_paginate')
    .css("margin-right", "15px")
  
  $('#alumnos_length, #alumnos_info')
    .css("margin-left", "15px")
  
  $("#alumnos")
    .css("width", "100%")
    .css("border-bottom", "1px solid #ddd")
    .css("border-top", "1px solid #ddd")
    .css('font-size', 'medium')
  
  $('#alumnos_wrapper')
    .css('padding-top', '10px')
    .css('padding-bottom', '10px')
    .css('font-size', '14px')
}

function defContent() {
  return '<div class="btn-group btn-group-xs" role="group" aria-label="...">' + 
    '<button data-toggle="modal" data-target="#alumno" type="button" class="btn btn-default"><i class="fa fa-info"></i></button> ' +
    '<a type="button" class="btn btn-default"><i class="fa fa-area-chart"></i></a></div>';
}

function defSelect() {
  return '<label><strong>&nbsp;&nbsp;Institucion:</strong> ' +
    '<select aria-controls="periodos" class="form-control input-sm"><option value="">Todos</option></select></label>';
}

function refreshModal(id) {
  $.getJSON("alumnos/" + id + "/json", function(data) {
    if (data) {
      renderModal(data[0])
    }
  })
}

function renderModal(data) {
  $.getJSON("institucion/json", function(data) {
    $('.modal select[name="ins"] option').attr('disabled', true)
    $('.modal select[name="ins"] option').remove()
    $.each(data, function(i, e) {
      $('.modal select[name="ins"]').append('<option value="' + e.institucion_id +'"> ' + e.institucion_nombre + '</option>')
    })
    $('.modal select[name="ins"] option').attr('disabled', false)
  })
  $(".modal input, .modal select").val('')
  $('.modal input[name="id"]').val(data.usuario_id)
  $('.modal input[name="rut"]').val(data.usuario_rut)
  $('.modal input[name="nya"]').val(data.usuario_nombre + ' ' + data.usuario_appat + ' ' + data.usuario_apmat)
  $('.modal input[name="mai"]').val(data.usuario_mail)
  $('.modal input[name="img"]').val('')
  $('.modal select[name="tip"]').val(data.tipo_id)
  $('.modal select[name="ins"]').val(data.institucion_id)
}